package com.mazeworks.linkapp;

import android.app.Application;
import android.os.Build;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.FirebaseDatabase;
import com.mazeworks.linkapp.util.ApplicationSettingsUtil;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by mambisiz on 9/3/17.
 */

public class LinkAppApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        EmojiManager.install(new IosEmojiProvider());
        JodaTimeAndroid.init(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ApplicationSettingsUtil.createNotificationChannels(this);
        }
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name(getString(R.string.db_name)).schemaVersion(22).deleteRealmIfMigrationNeeded() .build();
        Realm.setDefaultConfiguration(config);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

}
