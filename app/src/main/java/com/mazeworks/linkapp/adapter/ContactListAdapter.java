package com.mazeworks.linkapp.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.Contact;
import com.mazeworks.linkapp.ui.home.ContactListFragment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mambisiz on 9/21/17.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactItemViewHolder> {
    private Context mContext;
    private List<Contact> mContactList;
    private ContactListFragment.OnFragmentInteractionListener mListener;
    public ContactListAdapter(Context context, List<Contact> contactList,ContactListFragment.OnFragmentInteractionListener listener) {
        mContext = context;
        mContactList = contactList;
        mListener = listener;
    }

    @Override
    public ContactItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.contact_item, parent, false);
        return new ContactItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactItemViewHolder holder, int position) {
        final Contact contact = mContactList.get(position);
        RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.user_profile_placeholder).fallback(R.drawable.user_profile_placeholder);
        Glide.with(mContext).load(contact.getImageUrl()).apply(requestOptions).into(holder.mAvatarImage);
        holder.mContactName.setText(contact.getName());
        holder.mContactPhoneNumber.setText(contact.getPhone());
        if(contact.isMessengerAvailable()){
            holder.mInviteButton.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onContactSelected(contact);
                }
            });
        }

    }

    @Override
    public void onViewRecycled(ContactItemViewHolder holder) {
        super.onViewRecycled(holder);
        holder.mInviteButton.setVisibility(View.VISIBLE);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    class ContactItemViewHolder extends RecyclerView.ViewHolder {
        final CircleImageView mAvatarImage;
        final AppCompatTextView mContactName,mContactPhoneNumber;
        final AppCompatButton mInviteButton;

        public ContactItemViewHolder(View itemView) {
            super(itemView);

            mAvatarImage = itemView.findViewById(R.id.avImage);
            mContactName = itemView.findViewById(R.id.contactName);
            mContactPhoneNumber= itemView.findViewById(R.id.contactPhoneNumber);
            mInviteButton = itemView.findViewById(R.id.inviteButton);
        }
    }
}
