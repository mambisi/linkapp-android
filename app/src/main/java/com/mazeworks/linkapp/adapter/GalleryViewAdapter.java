package com.mazeworks.linkapp.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mazeworks.linkapp.model.GalleryItem;
import com.mazeworks.linkapp.ui.chat.PhotoViewerFragment;

import io.realm.RealmResults;

/**
 * Created by mambisiz on 9/16/17.
 */

public class GalleryViewAdapter extends FragmentStatePagerAdapter {
    private RealmResults<GalleryItem> mGalleryItems;
    private Context mContext;

    public GalleryViewAdapter(FragmentManager fm, RealmResults<GalleryItem> galleryItems, Context context) {
        super(fm);
        mGalleryItems = galleryItems;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return PhotoViewerFragment.newInstance(mGalleryItems.get(position).getUrl());
    }

    @Override
    public int getCount() {
        return mGalleryItems.size();
    }
}
