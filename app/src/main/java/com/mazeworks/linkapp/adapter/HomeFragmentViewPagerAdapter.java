package com.mazeworks.linkapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mazeworks.linkapp.ui.home.MessageListFragment;
import com.mazeworks.linkapp.ui.home.UserProfileFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mambisiz on 9/3/17.
 */

public class HomeFragmentViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<>();
    private String[] titles = new String[]{"Profile", "Messages"};
    public HomeFragmentViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentList.add(UserProfileFragment.newInstance());
        mFragmentList.add(MessageListFragment.newInstance());
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

}
