package com.mazeworks.linkapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.ChatRoom;
import com.mazeworks.linkapp.model.Contact;
import com.mazeworks.linkapp.model.User;
import com.mazeworks.linkapp.ui.chat.ChatActivity;
import com.mazeworks.linkapp.ui.chat.ChatFragment;
import com.mazeworks.linkapp.ui.home.ChatChannelItemMenuDialogFragment;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.Date;
import java.util.Map;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by mambisiz on 9/4/17.
 */

public class MessageChannelsListAdapter extends FirebaseRecyclerAdapter<RecyclerView.ViewHolder, ChatRoom> {

    private Context mContext;
    private Realm mRealm;

    public MessageChannelsListAdapter(Query query, Class<ChatRoom> itemClass, Context context) {
        super(query, itemClass);
        mContext = context;
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public int getItemViewType(int position) {
        ChatRoom chatRoom = getItem(position);
        if (chatRoom.getReceiverChannelId() != null) {
            return 100;
        }
        return 200;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 200) {
            View view1 = LayoutInflater.from(mContext).inflate(R.layout.message_channel_list_item, parent, false);
            return new MessageChannelItemViewHolder(view1);
        } else if (viewType == 100) {
            View view2 = LayoutInflater.from(mContext).inflate(R.layout.message_channel_list_item2, parent, false);
            return new MessageChannelItemViewHolder2(view2);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == 200)
            createQuickChatChannel((MessageChannelItemViewHolder) holder, position);
        else if (holder.getItemViewType() == 100) {
            MessageChannelItemViewHolder2 channelItemViewHolder2 = (MessageChannelItemViewHolder2) holder;
            createFriendChat(position, channelItemViewHolder2);
        }
    }

    private void createFriendChat(int position, final MessageChannelItemViewHolder2 holder) {
        final ChatRoom chatRoom = getItem(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChatRoom(holder.mChatTopic.getText().toString(),chatRoom);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showChatOptionsMenu(chatRoom);
                return true;
            }
        });
        if (chatRoom.getCurrentMessage() != null) {
            String desc;
            Map<String, String> mp = chatRoom.getCurrentMessage();
            RealmQuery<Contact> conRealm = mRealm.where(Contact.class);
            final Contact contact = conRealm.equalTo("formattedPhoneNumber", mp.get("phone")).findFirst();

            if (contact == null)
                if (mp.containsKey("phone"))
                    desc = mp.get("body");
                else
                    desc = mp.get("body");
            else
                desc = mp.get("body");
            holder.mChatDesc.setText(Html.fromHtml(desc));
        }
        if (chatRoom.getCreatorUID() != null) {
            final Contact contact1 = mRealm.where(Contact.class).equalTo("uid", chatRoom.getCreatorUID()).findFirst();
            if (contact1 != null) {
                holder.mChatTopic.setText(contact1.getName());
                Glide.with(mContext).load(contact1.getImageUrl()).into(holder.mChatImage);
            } else {
                FirebaseDatabase.getInstance().getReference("users").child(chatRoom.getCreatorUID()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user != null) {
                            holder.mChatTopic.setText(user.getPhoneNumber());
                            RequestOptions requestOptions = new RequestOptions().circleCrop();
                            Glide.with(mContext).load(user.getProfileUrl()).apply(requestOptions).into(holder.mChatImage);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    private void createQuickChatChannel(final MessageChannelItemViewHolder holder, int position) {
        holder.mChatProgress.setProgress(0);
        final ChatRoom chatRoom = getItem(position);

        if (chatRoom.getColorPalette().containsKey("color100") && chatRoom.getColorPalette().containsKey("color700")) {
            GradientDrawable shapeDrawable = (GradientDrawable) holder.mChatIcon.getBackground();
            String color100 = "#" + chatRoom.getColorPalette().get("color100");
            String color700 = "#" + chatRoom.getColorPalette().get("color700");
            shapeDrawable.setColorFilter(Color.parseColor(color100), PorterDuff.Mode.SRC_OVER);
            holder.mChatIcon.setColorFilter(Color.parseColor(color700), PorterDuff.Mode.SRC_ATOP);
        }

        Glide.with(mContext).load(chatRoom.getImageUrl()).into(holder.mChatImage);
        holder.mChatTopic.setText(chatRoom.getTopic());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChatRoom(holder.mChatTopic.getText().toString(),chatRoom);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showChatOptionsMenu(chatRoom);
                return true;
            }
        });
        holder.mChatDesc.setText(chatRoom.getDesc());
        double timeComplete = (chatRoom.getEndTimestamp() - chatRoom.getStartTimestamp());
        double timeElapsed = (new Date().getTime() - chatRoom.getStartTimestamp());
        float completed = (float) ((timeElapsed / timeComplete) * 100);


        int animationDuration = 2500; // 2500ms = 2,5s
        if (completed >= 100f) {
            completed = 100f;
            holder.mChatProgress.setColor(Color.parseColor("#bdbdbd"));
            holder.mChatProgress.setProgress(completed);
        } else {
            holder.mChatProgress.setProgressWithAnimation(completed, animationDuration);
        }
        if (chatRoom.getCurrentMessage() == null) {
            FirebaseDatabase.getInstance().getReference("channels").child(chatRoom.getChannelId()).child("currentMessage").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String, String> mp = (Map<String, String>) dataSnapshot.getValue();
                    String desc = null;
                    if (mp != null) {
                        desc = mp.get("body");
                        holder.mChatDesc.setText(Html.fromHtml(desc));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {

            String desc = null;

            if (chatRoom.getCurrentMessage() != null) {
                Map<String, String> mp = chatRoom.getCurrentMessage();
                RealmQuery<Contact> conRealm = mRealm.where(Contact.class);
                Contact contact = conRealm.equalTo("formattedPhoneNumber", mp.get("phone")).findFirst();

                if (contact == null)
                    if (mp.containsKey("phone"))
                        desc = mp.get("body");
                    else
                        desc = mp.get("body");
                else
                    desc = mp.get("body");
                holder.mChatDesc.setText(Html.fromHtml(desc));
            }

        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);

        if (holder.getItemViewType() == 200)
            ((MessageChannelItemViewHolder) holder).mChatProgress.setColor(Color.parseColor("#00E676"));
        else if (holder.getItemViewType() == 100)
            ((MessageChannelItemViewHolder2) holder).mChatImage.setImageDrawable(null);
    }

    @Override
    protected void itemAdded(ChatRoom item, String key, int position) {

    }

    @Override
    protected void itemChanged(ChatRoom oldItem, ChatRoom newItem, String key, int position) {

    }

    @Override
    protected void itemRemoved(ChatRoom item, String key, int position) {

    }

    @Override
    protected void itemMoved(ChatRoom item, String key, int oldPosition, int newPosition) {

    }

    private void showChatRoom(String title,ChatRoom chatRoom) {

        if (mContext instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) mContext;
            /*FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("Messenger");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.
            DialogFragment newFragment = ChatFragment.newInstance(title,chatRoom);
            newFragment.show(ft, "Messenger");*/
            Intent intent = new Intent(mContext,ChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("title", title);
            bundle.putSerializable("chatRoom", chatRoom);
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }

    }

    private void showChatOptionsMenu(ChatRoom chatRoom) {
        if (mContext instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) mContext;
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("CHAT_MENU");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.
            DialogFragment newFragment = ChatChannelItemMenuDialogFragment.newInstance(chatRoom);
            newFragment.show(ft, "CHAT_MENU");
        }

    }

    class MessageChannelItemViewHolder extends RecyclerView.ViewHolder {

        CircleImageView mChatImage;
        AppCompatTextView mChatTopic, mChatDesc;
        CircularProgressBar mChatProgress;
        AppCompatImageView mChatIcon;

        public MessageChannelItemViewHolder(View itemView) {
            super(itemView);

            mChatImage = itemView.findViewById(R.id.chatImage);
            mChatIcon = itemView.findViewById(R.id.chatIcon);
            mChatTopic = itemView.findViewById(R.id.chatTopic);
            mChatDesc = itemView.findViewById(R.id.chatDesc);
            mChatProgress = itemView.findViewById(R.id.chatProgress);

        }
    }

    class MessageChannelItemViewHolder2 extends RecyclerView.ViewHolder {

        CircleImageView mChatImage;
        AppCompatTextView mChatTopic, mChatDesc;
        AppCompatTextView mChatProgress;

        public MessageChannelItemViewHolder2(View itemView) {
            super(itemView);

            mChatImage = itemView.findViewById(R.id.chatImage);
            mChatTopic = itemView.findViewById(R.id.chatTopic);
            mChatDesc = itemView.findViewById(R.id.chatDesc);
            mChatProgress = itemView.findViewById(R.id.chatProgress);

        }
    }
}
