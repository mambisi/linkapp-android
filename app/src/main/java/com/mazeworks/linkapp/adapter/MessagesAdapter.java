package com.mazeworks.linkapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.ChangeBounds;
import android.support.transition.ChangeImageTransform;
import android.support.transition.ChangeTransform;
import android.support.transition.Fade;
import android.support.transition.TransitionSet;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.Query;
import com.mazeworks.linkapp.R;

import com.mazeworks.linkapp.model.GalleryItem;
import com.mazeworks.linkapp.model.Message;
import com.mazeworks.linkapp.ui.chat.ChatGalleryFragment;

import com.mypopsy.maps.StaticMap;


import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.WeakHashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by mambisiz on 9/6/17.
 */

public class MessagesAdapter extends FirebaseRecyclerAdapter<RecyclerView.ViewHolder, Message> {
    private Context mContext;
    private AdapterCallbacks mAdapterCallbacks;
    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private WeakHashMap<String, Drawable> imageCache = new WeakHashMap<>();
    private static final int VIEW_TYPE_MESSAGE_FROM_OTHERS = 100;
    private static final int VIEW_TYPE_IMAGE_MESSAGE_FROM_OTHERS = 101;
    private static final int VIEW_TYPE_LOCATION_MESSAGE_FROM_OTHERS = 103;
    private static final int VIEW_TYPE_USER_MESSAGE = 200;
    private static final int VIEW_TYPE_IMAGE_MESSAGE_BY_USER = 201;
    private static final int VIEW_TYPE_LOCATION_MESSAGE_BY_USER = 203;
    private int width;
    private String mChannelId;
    private int height;
    private Realm mRealm;
    private GeoDataClient mGeoDataClient;

    public MessagesAdapter(Query query, Class<Message> itemClass, Context context, String channelId, AdapterCallbacks adapterCallbacks) {
        super(query, itemClass);
        imageCache.clear();
        mContext = context;
        if (mFirebaseUser == null) {
            try {
                throw new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mGeoDataClient = Places.getGeoDataClient(context, null);
        mChannelId = channelId;
        mAdapterCallbacks = adapterCallbacks;

        Display display = ((AppCompatActivity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        mRealm = Realm.getDefaultInstance();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_MESSAGE_FROM_OTHERS:
                View view = LayoutInflater.from(mContext).inflate(R.layout.message_item, parent, false);
                return new MessageItemViewHolder(view);
            case VIEW_TYPE_USER_MESSAGE:
                View view1 = LayoutInflater.from(mContext).inflate(R.layout.message_item_by_user, parent, false);
                return new MessageItemSentByUserViewHolder(view1);
            case VIEW_TYPE_LOCATION_MESSAGE_BY_USER:
                View view5 = LayoutInflater.from(mContext).inflate(R.layout.message_item_by_user_with_location, parent, false);
                return new LocationMessageItemSentByUserViewHolder(view5);
            case VIEW_TYPE_IMAGE_MESSAGE_BY_USER:
                View view3 = LayoutInflater.from(mContext).inflate(R.layout.message_item_by_user_with_image_attachment, parent, false);
                return new ImageMessageByUserViewHolder(view3);
            case VIEW_TYPE_IMAGE_MESSAGE_FROM_OTHERS:
                View view4 = LayoutInflater.from(mContext).inflate(R.layout.message_item_by_other_with_image_attachment, parent, false);
                return new ImageMessageByOtherViewHolder(view4);
            case VIEW_TYPE_LOCATION_MESSAGE_FROM_OTHERS:
                View view6 = LayoutInflater.from(mContext).inflate(R.layout.message_item_by_other_with_location, parent, false);
                return new LocationMessageItemSentByOtherViewHolder(view6);
            default:
                View view2 = LayoutInflater.from(mContext).inflate(R.layout.message_item_by_user, parent, false);
                return new MessageItemSentByUserViewHolder(view2);
        }
    }


    @Override
    public int getItemViewType(int position) {
        Message message = getItem(position);
        if (message.getAuthorId().contentEquals(mFirebaseUser.getUid())) {
            if (message.getAttachments().containsKey("IMAGE"))
                return VIEW_TYPE_IMAGE_MESSAGE_BY_USER;
            else if (message.getAttachments().containsKey("LOCATION")) {
                return VIEW_TYPE_LOCATION_MESSAGE_BY_USER;
            }
            return VIEW_TYPE_USER_MESSAGE;
        } else {
            if (message.getAttachments().containsKey("IMAGE")) {
                return VIEW_TYPE_IMAGE_MESSAGE_FROM_OTHERS;
            } else if (message.getAttachments().containsKey("LOCATION")) {
                return VIEW_TYPE_LOCATION_MESSAGE_FROM_OTHERS;
            } else {
                return VIEW_TYPE_MESSAGE_FROM_OTHERS;
            }
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Message message = getItem(position);
        Date date = new Date(message.getTimestamp());


        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        DateFormat outputFormat = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
        String formattedTime = outputFormat.format(cal.getTime());

        Boolean isFollowUpMessage = isFollowUpMessageChecker(position, message);


        int size16DP = 20;
        int size4DP = 1;

        int margin16Dp = createDpWith(size16DP);
        int margin4Dp = createDpWith(size4DP);

        switch (holder.getItemViewType()) {

            case VIEW_TYPE_MESSAGE_FROM_OTHERS:
                holder.itemView.setClickable(false);
                createInboundMessageView((MessageItemViewHolder) holder, message,
                        formattedTime,
                        isFollowUpMessage,
                        margin16Dp, margin4Dp);
                break;
            case VIEW_TYPE_USER_MESSAGE:
                holder.itemView.setClickable(false);
                createOutBoundMessageView((MessageItemSentByUserViewHolder) holder, message,
                        formattedTime,
                        isFollowUpMessage, margin16Dp, margin4Dp);
                break;
            case VIEW_TYPE_LOCATION_MESSAGE_BY_USER:
                createOutboundLocationMessage((LocationMessageItemSentByUserViewHolder) holder, message);
                break;

            case VIEW_TYPE_IMAGE_MESSAGE_BY_USER:
                holder.itemView.setClickable(true);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showGallery(message, ((ImageMessageByUserViewHolder) holder).mImageMessage);
                    }
                });
                createOutboundImageMessage((ImageMessageByUserViewHolder) holder, message);
                break;
            case VIEW_TYPE_IMAGE_MESSAGE_FROM_OTHERS:
                holder.itemView.setClickable(true);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showGallery(message, ((ImageMessageByOtherViewHolder) holder).mImageMessage);
                    }
                });
                createInboundImageMessage((ImageMessageByOtherViewHolder) holder, message);
                break;
            case VIEW_TYPE_LOCATION_MESSAGE_FROM_OTHERS:
                createInboundLocationMessage((LocationMessageItemSentByOtherViewHolder) holder, message);
                break;

        }


    }

    private void createInboundLocationMessage(LocationMessageItemSentByOtherViewHolder holder, final Message message) {
        final double lat2 = (double) message.getAttachmentMetadata().get("latitude");
        final double lng2 = (double) message.getAttachmentMetadata().get("longitude");


        LocationMessageItemSentByOtherViewHolder locationMessageItemSentByOtherViewHolder = holder;
        locationMessageItemSentByOtherViewHolder.mMessageBody.setText(message.getAttachments().get("LOCATION"));
        locationMessageItemSentByOtherViewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMap(lat2, lng2, message.getAuthorName());
            }
        });
        Glide.with(mContext).load(message.getAuthorProfileUrl()).into(locationMessageItemSentByOtherViewHolder.mAvatarImage);
        StaticMap staticMap2 = new StaticMap().size(320, 240);
        try {
            URL mapImageUrl = staticMap2.key(mContext.getString(R.string.google_services_api_key)).marker(new StaticMap.GeoPoint(lat2, lng2)).toURL();
            Glide.with(mContext).load(mapImageUrl).into(locationMessageItemSentByOtherViewHolder.mLocationImage);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void createOutboundLocationMessage(LocationMessageItemSentByUserViewHolder holder, final Message message) {
        final double lat = (double) message.getAttachmentMetadata().get("latitude");
        final double lng = (double) message.getAttachmentMetadata().get("longitude");


        LocationMessageItemSentByUserViewHolder locationMessageItemSentByUserViewHolder = holder;
        locationMessageItemSentByUserViewHolder.mMessageBody.setText(message.getAttachments().get("LOCATION"));
        locationMessageItemSentByUserViewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMap(lat, lng, message.getAuthorName());
            }
        });
        StaticMap staticMap = new StaticMap().size(320, 240);
        try {
            URL mapImageUrl = staticMap.key(mContext.getString(R.string.google_services_api_key)).marker(new StaticMap.GeoPoint(lat, lng)).toURL();
            Glide.with(mContext).load(mapImageUrl).into(locationMessageItemSentByUserViewHolder.mLocationImage);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


    private void showMap(double lat, double lng, String authorName) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(
                "geo:" + lat +
                        "," + lng +
                        "?q=" + lat +
                        "," + lng +
                        "(" + "Shared by " + authorName + ")"));

        //intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        mContext.startActivity(intent);

    }

    private void createOutboundImageMessage(final ImageMessageByUserViewHolder holder, Message message) {
        double h1 = (double) ((long) message.getAttachmentMetadata().get("height"));
        double w1 = (double) ((long) message.getAttachmentMetadata().get("width"));

        int h2 = 0;
        int w2 = 800;

        h2 = (int) (((h1 / w1) * w2));
        if (h2 > 800) {
            h2 = 800;
        }

        RequestOptions requestOptions;

        ImageMessageByUserViewHolder imageMessageByUserViewHolder = holder;
        imageMessageByUserViewHolder.mAvatarImage.setImageDrawable(null);
        Glide.with(mContext).load(message.getAuthorProfileUrl()).into(imageMessageByUserViewHolder.mAvatarImage);


        if (message.getImagePlaceHolder() != null) {
            Drawable drawable;
            if (imageCache.containsKey(message.getMessageId())) {
                drawable = imageCache.get(message.getMessageId());
            } else {
                byte[] decodedString = Base64.decode(message.getImagePlaceHolder(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                drawable = new BitmapDrawable(decodedByte);
                imageCache.put(message.getMessageId(), drawable);
            }


            requestOptions = new RequestOptions().placeholder(drawable).override(w2, h2).centerCrop();
        } else {
            requestOptions = new RequestOptions().placeholder(R.drawable.square_back).override(w2, h2).centerCrop();
        }
        String imageUrl = message.getAttachments().get("IMAGE");
        Glide.with(mContext).load(imageUrl).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.mProgressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.mProgressBar.setVisibility(View.GONE);
                return false;
            }
        }).apply(requestOptions).into(imageMessageByUserViewHolder.mImageMessage);
    }

    private void createInboundImageMessage(final ImageMessageByOtherViewHolder holder, Message message) {
        double h1 = (double) ((long) message.getAttachmentMetadata().get("height"));
        double w1 = (double) ((long) message.getAttachmentMetadata().get("width"));

        int h2 = 0;
        int w2 = 800;

        h2 = (int) (((h1 / w1) * w2));
        if (h2 > 800) {
            h2 = 800;
        }

        RequestOptions requestOptions2;

        if (message.getImagePlaceHolder() != null) {

            Drawable drawable;
            if (imageCache.containsKey(message.getMessageId())) {
                drawable = imageCache.get(message.getMessageId());
            } else {
                byte[] decodedString = Base64.decode(message.getImagePlaceHolder(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                drawable = new BitmapDrawable(decodedByte);
                imageCache.put(message.getMessageId(), drawable);
            }


            requestOptions2 = new RequestOptions().placeholder(drawable).override(w2, h2).centerCrop();
        } else {
            requestOptions2 = new RequestOptions().placeholder(R.drawable.square_back).override(w2, h2).centerCrop();
        }

        if (message.getColor() != null) {
            holder.mAuthorName.setTextColor(Color.parseColor(message.getColor()));
        }
        holder.mAvatarImage.setImageDrawable(null);
        Glide.with(mContext).load(message.getAuthorProfileUrl()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.mProgressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.mProgressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.mAvatarImage);
        String imageUrl2 = message.getAttachments().get("IMAGE");
        Glide.with(mContext).load(imageUrl2).apply(requestOptions2).into(holder.mImageMessage);
        holder.mAuthorName.setText(message.getAuthorName().toLowerCase());
    }

    private void createOutBoundMessageView(MessageItemSentByUserViewHolder messageItemSentByUserViewHolder,
                                           Message message, String formattedTime, Boolean isFollowUpMessage,
                                           int margin16Dp, int margin4Dp) {
        int len = message.getBody().length();
        if (len == 2) {
            messageItemSentByUserViewHolder.mMessageBody.setTextSize(40);
        } else {
            messageItemSentByUserViewHolder.mMessageBody.setTextSize(15);
        }
        System.out.println("LENGTH: " + len);
        if (!isFollowUpMessage) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) messageItemSentByUserViewHolder.mCardView.getLayoutParams();
            params.topMargin = margin16Dp;

        } else {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) messageItemSentByUserViewHolder.mCardView.getLayoutParams();
            params.topMargin = margin4Dp;
        }

        messageItemSentByUserViewHolder.mTimeStamp.setText(formattedTime);
        messageItemSentByUserViewHolder.mMessageBody.setText(message.getBody());
    }


    private void createInboundMessageView(MessageItemViewHolder messageItemViewHolder,
                                          Message message, String formattedTime, Boolean isFollowUpMessage,
                                          int margin16Dp, int margin4Dp) {
        messageItemViewHolder.mAvatarImage.setImageDrawable(null);
        messageItemViewHolder.mAuthorName.setText(message.getAuthorName().toLowerCase());
        int len = message.getBody().length();
        if (len == 2) {
            messageItemViewHolder.mMessageBody.setTextSize(40);
        } else {
            messageItemViewHolder.mMessageBody.setTextSize(15);
        }

        if (message.getBody().toCharArray().length < 1) {
            messageItemViewHolder.mMessageBody.setTextSize(40);
        }

        if (message.getColor() != null) {
            messageItemViewHolder.mAuthorName.setTextColor(Color.parseColor(message.getColor()));
        }

        if (!isFollowUpMessage) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) messageItemViewHolder.mCardView.getLayoutParams();
            params.topMargin = margin16Dp;
            Glide.with(mContext).load(message.getAuthorProfileUrl()).into(messageItemViewHolder.mAvatarImage);
        } else {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) messageItemViewHolder.mCardView.getLayoutParams();
            params.topMargin = margin4Dp;
        }

        messageItemViewHolder.mTimeStamp.setText(formattedTime);


        messageItemViewHolder.mMessageBody.setText(message.getBody());
    }

    private int createDpWith(int size4DP) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, size4DP, mContext.getResources()
                        .getDisplayMetrics());
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        switch (holder.getItemViewType()) {

            case VIEW_TYPE_MESSAGE_FROM_OTHERS:
                ((MessageItemViewHolder) holder).mMessageBody.setTextSize(15);
                break;
            case VIEW_TYPE_USER_MESSAGE:
                ((MessageItemSentByUserViewHolder) holder).mMessageBody.setTextSize(15);
                break;
            case VIEW_TYPE_IMAGE_MESSAGE_FROM_OTHERS:
                ((ImageMessageByOtherViewHolder) holder).mProgressBar.setVisibility(View.VISIBLE);
                break;
            case VIEW_TYPE_IMAGE_MESSAGE_BY_USER:
                ((ImageMessageByUserViewHolder) holder).mProgressBar.setVisibility(View.VISIBLE);
                break;
        }

    }

    @NonNull
    private Boolean isFollowUpMessageChecker(int position, Message message) {
        int de = position - 1;
        Boolean isFollowUpMessage = false;
        if (de == -1) {
            System.out.println(message);
            isFollowUpMessage = false;
        } else if (de >= 0) {
            //Check follow up
            Message messageTop = getItem(de);
            if (messageTop.getAuthorId().contentEquals(message.getAuthorId())) {
                long interval = Math.abs(messageTop.getTimestamp() - message.getTimestamp());
                isFollowUpMessage = interval < 50000;
            } else {
                isFollowUpMessage = false;
            }

        }
        return isFollowUpMessage;
    }

    @Override
    protected void itemAdded(Message item, String key, int position) {
        mAdapterCallbacks.onItemAdd();
        if (item.getAttachments().containsKey("IMAGE")) {
            addImageToGallery(item);
        }
    }

    @Override
    protected void itemChanged(Message oldItem, Message newItem, String key, int position) {

    }

    @Override
    protected void itemRemoved(Message item, String key, int position) {

    }

    @Override
    protected void itemMoved(Message item, String key, int oldPosition, int newPosition) {

    }

    // <editor-fold defaultstate="collapsed" desc="This where all the View holders are de-cleared and initiated ">

    class MessageItemViewHolder extends RecyclerView.ViewHolder {

        final AppCompatTextView mMessageBody, mTimeStamp, mAuthorName;
        final CircleImageView mAvatarImage;
        final CardView mCardView;

        MessageItemViewHolder(View itemView) {
            super(itemView);
            mMessageBody = itemView.findViewById(R.id.msgBody);
            mTimeStamp = itemView.findViewById(R.id.timeStamp);
            mCardView = itemView.findViewById(R.id.cardView);
            mAvatarImage = itemView.findViewById(R.id.avImage);
            mAuthorName = itemView.findViewById(R.id.authorName);
        }
    }

    class MessageItemSentByUserViewHolder extends RecyclerView.ViewHolder {

        final AppCompatTextView mMessageBody, mTimeStamp;
        final CardView mCardView;

        MessageItemSentByUserViewHolder(View itemView) {
            super(itemView);
            mMessageBody = itemView.findViewById(R.id.msgBody);
            mTimeStamp = itemView.findViewById(R.id.timeStamp);
            mCardView = itemView.findViewById(R.id.cardView);
        }
    }

    class LocationMessageItemSentByUserViewHolder extends RecyclerView.ViewHolder {

        final AppCompatTextView mMessageBody, mTimeStamp;
        final AppCompatImageView mLocationImage;
        final CardView mCardView;

        LocationMessageItemSentByUserViewHolder(View itemView) {
            super(itemView);
            mMessageBody = itemView.findViewById(R.id.msgBody);
            mTimeStamp = itemView.findViewById(R.id.timeStamp);
            mCardView = itemView.findViewById(R.id.cardView);
            mLocationImage = itemView.findViewById(R.id.locationImage);
        }
    }

    class LocationMessageItemSentByOtherViewHolder extends RecyclerView.ViewHolder {

        final AppCompatTextView mMessageBody, mTimeStamp;
        final AppCompatImageView mLocationImage;
        final CircleImageView mAvatarImage;
        final CardView mCardView;

        LocationMessageItemSentByOtherViewHolder(View itemView) {
            super(itemView);
            mMessageBody = itemView.findViewById(R.id.msgBody);
            mTimeStamp = itemView.findViewById(R.id.timeStamp);
            mCardView = itemView.findViewById(R.id.cardView);
            mAvatarImage = itemView.findViewById(R.id.avImage);
            mLocationImage = itemView.findViewById(R.id.locationImage);
        }
    }

    class ImageMessageByUserViewHolder extends RecyclerView.ViewHolder {
        final ProgressBar mProgressBar;
        final AppCompatTextView mTimeStamp, mAuthorName;
        final AppCompatImageView mImageMessage;
        final CircleImageView mAvatarImage;

        public ImageMessageByUserViewHolder(View itemView) {
            super(itemView);
            mImageMessage = itemView.findViewById(R.id.messageImage);
            mAvatarImage = itemView.findViewById(R.id.avImage);
            mAuthorName = itemView.findViewById(R.id.authorName);
            mTimeStamp = itemView.findViewById(R.id.timeStamp);
            mProgressBar = itemView.findViewById(R.id.progressBar2);
        }
    }

    class ImageMessageByOtherViewHolder extends RecyclerView.ViewHolder {
        final AppCompatTextView mTimeStamp, mAuthorName;
        final AppCompatImageView mImageMessage;
        final ProgressBar mProgressBar;
        final CircleImageView mAvatarImage;

        public ImageMessageByOtherViewHolder(View itemView) {
            super(itemView);
            mImageMessage = itemView.findViewById(R.id.messageImage);
            mAvatarImage = itemView.findViewById(R.id.avImage);
            mAuthorName = itemView.findViewById(R.id.authorName);
            mTimeStamp = itemView.findViewById(R.id.timeStamp);
            mProgressBar = itemView.findViewById(R.id.progressBar2);
        }
    }

    // </editor-fold>


    public interface AdapterCallbacks {
        void onItemAdd();
    }


    private void addImageToGallery(Message message) {
        final GalleryItem galleryItem = new GalleryItem();
        galleryItem.setChannelId(mChannelId);
        galleryItem.setMessageId(message.getMessageId());
        galleryItem.setType("IMAGE");
        galleryItem.setUrl(message.getAttachments().get("IMAGE"));

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // This will create a new object in Realm or throw an exception if the
                // object already exists (same primary key)
                // realm.copyToRealm(obj);

                // This will update an existing object with the same primary key
                // or create a new object if an object with no primary key = 42
                realm.copyToRealmOrUpdate(galleryItem);
            }
        });
    }

    private void showGallery(Message message, View view) {

        DialogFragment newFragment = ChatGalleryFragment.newInstance(mChannelId, message.getMessageId());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            newFragment.setSharedElementEnterTransition(new DetailsTransition());
            newFragment.setEnterTransition(new Fade());
            newFragment.setExitTransition(new Fade());
            view.setTransitionName(message.getMessageId());
            newFragment.setSharedElementReturnTransition(new DetailsTransition());
        }
        if (mContext instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) mContext;
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.addSharedElement(view, message.getMessageId());
            Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("GALLERY");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            // Create and show the dialog.
            ;
            newFragment.show(ft, "GALLERY");
            // newFragment.getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }

    }

    class DetailsTransition extends TransitionSet {
        public DetailsTransition() {
            setOrdering(ORDERING_TOGETHER);
            addTransition(new ChangeBounds()).
                    addTransition(new ChangeTransform()).
                    addTransition(new ChangeImageTransform());
        }
    }

}

