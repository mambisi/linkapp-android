package com.mazeworks.linkapp.model;

/**
 * Created by mambisiz on 9/4/17.
 */

public class AddChatDialogClosedEvent {

    public final String message;

    public AddChatDialogClosedEvent(String message){
        this.message = message;
    }
}
