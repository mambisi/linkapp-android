package com.mazeworks.linkapp.model;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

/**
 * Created by mambisiz on 11/21/17.
 */

public class AudioMessage extends Message {
    private FirebaseStorage mFirebaseStorage = FirebaseStorage.getInstance();

    public void sendAudioMessage(final Context context, Uri audio, final String chatRoomId) {
        final FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        final Message message = new Message();
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoomId)
                .child("messages")
                .push();


        String pushId = databaseReference.getKey();
        message.setAuthorId(mFirebaseUser.getUid());
        message.setMessageId(pushId);
        message.setColor("");

        mFirebaseStorage.getReference("audioAttachments/" + pushId + "/" + UUID.randomUUID().toString() + ".mp3").putFile(audio).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (taskSnapshot.getDownloadUrl() != null) {
                    message.getAttachments().put("AUDIO", taskSnapshot.getDownloadUrl().toString());
                    message.setBody("🎤 audio message");
                    message.sendMessage(context, chatRoomId);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "Couldnt send audio", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sendAudioMessage(final Context context, Uri audio, final String chatRoomId, final String chatRoom2) {
        final FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        final Message message = new Message();
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoomId)
                .child("messages")
                .push();


        String pushId = databaseReference.getKey();
        message.setAuthorId(mFirebaseUser.getUid());
        message.setMessageId(pushId);

        mFirebaseStorage.getReference("audioAttachments/" + pushId + "/" + UUID.randomUUID().toString() + ".mp3").putFile(audio).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (taskSnapshot.getDownloadUrl() != null)
                    message.getAttachments().put("AUDIO", taskSnapshot.getDownloadUrl().toString());
                message.setBody("🎤 audio message");
                message.sendMessage(context, chatRoomId, chatRoom2);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "Couldnt send audio", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
