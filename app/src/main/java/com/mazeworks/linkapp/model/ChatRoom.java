package com.mazeworks.linkapp.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mambisiz on 9/4/17.
 */

public class ChatRoom implements Serializable{
    private String mChannelId;
    private String mImageUrl;
    private String mTopic;
    private String mDesc;
    private String mCreatorUID;
    private String mReceiverChannelId;
    private int mDuration;
    private int mMaxPeople;
    private long mStartTimestamp;
    private long mEndTimestamp;
    private long mCurrentMessageTimestamp;
    private boolean isConverted = false;
    private Map<String, String> mColorPalette =  new HashMap<>();
    private Map<String, String> mCurrentMessage;
    public ChatRoom() {
    }

    public ChatRoom(String channelId, String imageUrl, String topic, String desc, String creatorUID, int duration, int maxPeople) {
        mChannelId = channelId;
        mImageUrl = imageUrl;
        mTopic = topic;
        mDesc = desc;
        mCreatorUID = creatorUID;
        mDuration = duration;
        mMaxPeople = maxPeople;
    }

    public Map<String, String> getCurrentMessage() {
        return mCurrentMessage;
    }

    public void setCurrentMessage(Map<String, String> currentMessage) {
        mCurrentMessage = currentMessage;
    }

    public String getReceiverChannelId() {
        return mReceiverChannelId;
    }

    public void setReceiverChannelId(String receiverChannelId) {
        mReceiverChannelId = receiverChannelId;
    }


    public long getCurrentMessageTimestamp() {
        return mCurrentMessageTimestamp;
    }

    public void setCurrentMessageTimestamp(long currentMessageTimestamp) {
        mCurrentMessageTimestamp = currentMessageTimestamp;
    }

    public Map<String, String> getColorPalette() {
        return mColorPalette;
    }

    public void setColorPalette(Map<String, String> colorPalette) {
        mColorPalette = colorPalette;
    }

    public String getChannelId() {
        return mChannelId;
    }

    public void setChannelId(String channelId) {
        mChannelId = channelId;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getTopic() {
        return mTopic;
    }

    public void setTopic(String topic) {
        mTopic = topic;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getCreatorUID() {
        return mCreatorUID;
    }

    public void setCreatorUID(String creatorUID) {
        mCreatorUID = creatorUID;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public int getMaxPeople() {
        return mMaxPeople;
    }

    public void setMaxPeople(int maxPeople) {
        mMaxPeople = maxPeople;
    }

    public long getStartTimestamp() {
        return mStartTimestamp;
    }

    public void setStartTimestamp(long startTimestamp) {
        mStartTimestamp = startTimestamp;
    }

    public long getEndTimestamp() {
        return mEndTimestamp;
    }

    public void setEndTimestamp(long endTimestamp) {
        mEndTimestamp = endTimestamp;
    }

    public boolean isConverted() {
        return isConverted;
    }

    public void setConverted(boolean converted) {
        isConverted = converted;
    }
}
