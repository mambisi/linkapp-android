package com.mazeworks.linkapp.model;

/**
 * Created by mambisiz on 9/5/17.
 */

public class CloseCameraFragmentShowMessages {
    public final ChatRoom mChatRoom;

    public CloseCameraFragmentShowMessages(ChatRoom chatRoom){
        this.mChatRoom = chatRoom;
    }
}
