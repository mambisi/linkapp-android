package com.mazeworks.linkapp.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mambisiz on 9/16/17.
 */

public class ColorPalette{
    private String color100;
    private String color700;


    public ColorPalette(String color100, String color700){
        this.color100 = color100;
        this.color700 = color700;
    }

    public String getColor100(){
        return color100;
    }
    public String getColor700(){
        return color700;
    }

    public static ColorPalette getRandomColor(){
        List<ColorPalette> colors = new ArrayList<>();
        colors.add(new ColorPalette("80D8FF","0091EA"));
        colors.add(new ColorPalette("B9F6CA","00C853"));
        colors.add(new ColorPalette("FF9E80","FF3D00"));
        colors.add(new ColorPalette("FFD180","FF6D00"));
        colors.add(new ColorPalette("BBDEFB","2962FF"));
        colors.add(new ColorPalette("D1C4E9","651FFF"));
        colors.add(new ColorPalette("B2EBF2","00E5FF"));
        Random rand = new Random();
        int randomNum = rand.nextInt((colors.size()- 1) + 1);
        return colors.get(randomNum);
    }
}
