package com.mazeworks.linkapp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mambisiz on 9/21/17.
 */

public class Contact extends RealmObject {


    private String uid;

    private String name;

    private String imageUrl;

    private String phone;

    private boolean messengerAvailable = false;
    @PrimaryKey
    private String formattedPhoneNumber;


    public Contact() {
    }

    public Contact(String name, String formattedPhoneNumber) {
        this.name = name;
        this.formattedPhoneNumber = formattedPhoneNumber;
    }



    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isMessengerAvailable() {
        return messengerAvailable;
    }

    public void setMessengerAvailable(boolean messengerAvailable) {
        this.messengerAvailable = messengerAvailable;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public void setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
    }
}
