package com.mazeworks.linkapp.model;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.util.ImageBlurBase64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * Created by mambisiz on 11/21/17.
 */

public class ImageMessage extends Message {

    public void sendImageMessage(String imageBoas, final Context context, final String chatRoomId) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("messageColor", this.getColor());
        bundle.putString("imageUrl", imageBoas);
        bundle.putString("chatRoomId", chatRoomId);
        String messageColor = bundle.getString("messageColor");
        final Message message = new Message();
        message.setColor(messageColor);
        String imageUrl = bundle.getString("imageUrl");
        Uri imageUri = Uri.parse(imageUrl);
        Bitmap bitmap = null;
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            final int takeFlags = intent.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.

            if (Build.VERSION.SDK_INT >= 19) {
                context.getContentResolver().takePersistableUriPermission(imageUri, takeFlags);
            }

            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        final byte[] data = baos.toByteArray();
        message.getAttachmentMetadata().put("height", bitmap.getHeight());
        message.getAttachmentMetadata().put("width", bitmap.getWidth());
        message.getAttachmentMetadata().put("size", data.length);

        ImageBlurBase64 imageBlurBase64 = new ImageBlurBase64(context, bitmap, new ImageBlurBase64.ImageBlurCallback() {
            @Override
            public void onDoneWithBlur(String string) {
                message.setImagePlaceHolder(string);
                sendImageMessage(message, data, chatRoomId, context);
            }
        });
        imageBlurBase64.execute();




        /*int jobId = (int) new Date().getTime();
        Intent intent = new Intent(context, ImageJobIntentService.class);
        intent.putExtras(bundle);
        context.startService(intent);
        ImageJobIntentService.enqueueWork(context,intent);*/

        //uploadInForeground(imageBoas, context, chatRoomId);
    }

    public void sendImageMessage(String imageBoas, final Context context, final String chatRoom1, final String chatRoom2) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("messageColor", this.getColor());
        bundle.putString("imageUrl", imageBoas);
        bundle.putString("chatRoomId", chatRoom1);
        String messageColor = bundle.getString("messageColor");
        final Message message = new Message();
        message.setColor(messageColor);
        String imageUrl = bundle.getString("imageUrl");
        Uri imageUri = Uri.parse(imageUrl);
        Bitmap bitmap = null;
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            final int takeFlags = intent.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.

            if (Build.VERSION.SDK_INT >= 19) {
                context.getContentResolver().takePersistableUriPermission(imageUri, takeFlags);
            }

            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        final byte[] data = baos.toByteArray();
        message.getAttachmentMetadata().put("height", bitmap.getHeight());
        message.getAttachmentMetadata().put("width", bitmap.getWidth());
        message.getAttachmentMetadata().put("size", data.length);

        ImageBlurBase64 imageBlurBase64 = new ImageBlurBase64(context, bitmap, new ImageBlurBase64.ImageBlurCallback() {
            @Override
            public void onDoneWithBlur(String string) {
                message.setImagePlaceHolder(string);
                sendImageMessage(message, data, chatRoom1, chatRoom2, context);
            }
        });
        imageBlurBase64.execute();
    }

    private void sendImageMessage(final Message message, byte[] imageBoas, final String chatRoomId, final Context context) {
        final FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String defImage = context.getString(R.string.default_avatar_url);
        if (mFirebaseUser == null) {
            try {
                throw new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoomId)
                .child("messages")
                .push();


        String pushId = databaseReference.getKey();
        message.setAuthorId(mFirebaseUser.getUid());
        message.setMessageId(pushId);


        message.setAuthorName(mFirebaseUser.getDisplayName());
        message.setAuthorProfileUrl(mFirebaseUser.getPhotoUrl() == null ? defImage : mFirebaseUser.getPhotoUrl().toString());
        message.setTimestamp(new Date().getTime());
        message.setBody(context.getString(R.string.default_image_message_body) );

        UploadTask uploadTask = FirebaseStorage.getInstance()
                .getReference("imageAttachments/" + message.getMessageId() + "/" + UUID.randomUUID().toString())
                .putBytes(imageBoas);

        final int id = (int) new Date().getTime();


        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double i = taskSnapshot.getBytesTransferred();
                double j = taskSnapshot.getTotalByteCount();
                double f = (i / j) * 100L;
                int incr = (int) (f);

                showUploadNotification(id, incr, false, context);
            }
        });

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

        uploadTask.addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });

        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                showUploadNotification(id, 0, true, context);


                Uri fileUri = taskSnapshot.getDownloadUrl();
                String ffs = fileUri.toString();
                performSaveMessage(ffs, message, databaseReference, chatRoomId, mFirebaseUser);
            }
        });
    }

    private void sendImageMessage(final Message message, byte[] imageBoas, final String chatRoom1, final String chatRoom2, final Context context) {
        final FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String defImage = context.getString(R.string.default_avatar_url);
        if (mFirebaseUser == null) {
            try {
                throw new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoom1)
                .child("messages")
                .push();


        String pushId = databaseReference.getKey();
        message.setAuthorId(mFirebaseUser.getUid());
        message.setMessageId(pushId);


        message.setAuthorName(mFirebaseUser.getDisplayName());
        message.setAuthorProfileUrl(mFirebaseUser.getPhotoUrl() == null ? defImage : mFirebaseUser.getPhotoUrl().toString());
        message.setTimestamp(new Date().getTime());
        message.setBody(context.getString(R.string.default_image_message_body));

        UploadTask uploadTask = FirebaseStorage.getInstance()
                .getReference("imageAttachments/" + message.getMessageId() + "/" + UUID.randomUUID().toString())
                .putBytes(imageBoas);

        final int id = (int) new Date().getTime();


        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double i = taskSnapshot.getBytesTransferred();
                double j = taskSnapshot.getTotalByteCount();

                double k = j - i;
                double f = (i / j) * 100L;
                int incr = (int) (f);

                showUploadNotification(id, incr, false, context);
            }
        });

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

        uploadTask.addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });

        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                showUploadNotification(id, 0, true, context);


                Uri fileUri = taskSnapshot.getDownloadUrl();
                String ffs = fileUri.toString();
                performSaveMessage(ffs, message, chatRoom1, chatRoom2, mFirebaseUser);
            }
        });
    }

}
