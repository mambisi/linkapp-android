package com.mazeworks.linkapp.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.service.ImageJobIntentService;
import com.mazeworks.linkapp.service.ImageUploaderService;
import com.mazeworks.linkapp.service.UserLastSeenUpdaterService;
import com.mazeworks.linkapp.util.ApplicationSettingsUtil;
import com.mazeworks.linkapp.util.ImageBlurBase64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by mambisiz on 9/6/17.
 */

public class Message {
    private String mBody;
    private long mTimestamp;
    private String mAuthorId;
    private Map<String, String> mReadList = new HashMap<>();
    private String mMessageId;
    private Map<String, String> mLikeList = new HashMap<>();
    private Integer mMessageIndex = 0;
    private String authorProfileUrl;
    private String authorName;
    private String mColor;
    private String phone;
    private Map<String, String> mAttachments = new HashMap<>();
    private Map<String, Object> mAttachmentMetadata = new HashMap<>();
    private String mImagePlaceHolder;

    public String getImagePlaceHolder() {
        return mImagePlaceHolder;
    }

    public void setImagePlaceHolder(String imagePlaceHolder) {
        this.mImagePlaceHolder = imagePlaceHolder;
    }

    public Message(String body) {
        mBody = body;
    }

    public Message() {

    }

    public Map<String, Object> getAttachmentMetadata() {
        return mAttachmentMetadata;
    }

    public void setAttachmentMetadata(Map<String, Object> attachmentMetadata) {
        mAttachmentMetadata = attachmentMetadata;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Map<String, String> getAttachments() {
        return mAttachments;
    }

    public void setAttachments(Map<String, String> attachments) {
        mAttachments = attachments;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(long timestamp) {
        mTimestamp = timestamp;
    }

    public String getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(String authorId) {
        mAuthorId = authorId;
    }

    public Map<String, String> getReadList() {
        return mReadList;
    }

    public void setReadList(Map<String, String> readList) {
        mReadList = readList;
    }

    public String getMessageId() {
        return mMessageId;
    }

    public void setMessageId(String messageId) {
        mMessageId = messageId;
    }

    public Map<String, String> getLikeList() {
        return mLikeList;
    }

    public void setLikeList(Map<String, String> likeList) {
        mLikeList = likeList;
    }

    public Integer getMessageIndex() {
        return mMessageIndex;
    }

    public void setMessageIndex(Integer messageIndex) {
        mMessageIndex = messageIndex;
    }

    public String getAuthorProfileUrl() {
        return authorProfileUrl;
    }

    public void setAuthorProfileUrl(String authorProfileUrl) {
        this.authorProfileUrl = authorProfileUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void sendMessage(Context context, String chatRoomId) {
        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String defImage = context.getString(R.string.default_avatar_url);
        if (mFirebaseUser == null) {
            try {
                throw new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        if (mFirebaseUser.getPhoneNumber() != null)
            this.setPhone(mFirebaseUser.getPhoneNumber());

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoomId)
                .child("messages")
                .push();


        String pushId = databaseReference.getKey();
        this.setAuthorId(mFirebaseUser.getUid());
        this.setMessageId(pushId);
        this.setAuthorName(mFirebaseUser.getDisplayName());
        this.setAuthorProfileUrl(mFirebaseUser.getPhotoUrl() == null ? defImage : mFirebaseUser.getPhotoUrl().toString());
        this.setTimestamp(new Date().getTime());

        if (!this.getColor().isEmpty()) {
            databaseReference.setValue(Message.this);
            return;
        } else {
            FirebaseDatabase.getInstance()
                    .getReference("channel-messages")
                    .child(chatRoomId)
                    .child("people-color")
                    .child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userColor = dataSnapshot.getValue(String.class);
                        Message.this.setColor(userColor);

                    } else {
                        Message.this.setColor("#2196F3");
                    }
                    databaseReference.setValue(Message.this);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Message.this.setColor("#2196F3");
                    databaseReference.setValue(Message.this);
                }
            });
        }

    }

    public void sendMessage(Context context, String chatRoom1, String chatRoom2) {
        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String defImage = context.getString(R.string.default_avatar_url);
        if (mFirebaseUser == null) {
            try {
                throw new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }


        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoom1)
                .child("messages")
                .push();

        String pushId = databaseReference.getKey();
        this.setAuthorId(mFirebaseUser.getUid());
        this.setMessageId(pushId);
        if (mFirebaseUser.getPhoneNumber() != null)
            this.setPhone(mFirebaseUser.getPhoneNumber());
        this.setAuthorName(mFirebaseUser.getDisplayName());
        this.setAuthorProfileUrl(mFirebaseUser.getPhotoUrl() == null ? defImage : mFirebaseUser.getPhotoUrl().toString());
        this.setTimestamp(new Date().getTime());
        final Map<String, Object> chatSync = new HashMap<>();
        chatSync.put("channel-messages/" + chatRoom1 + "/messages/" + this.getMessageId(), this);
        chatSync.put("channel-messages/" + chatRoom2 + "/messages/" + this.getMessageId(), this);
        if (!this.getColor().isEmpty()) {

            FirebaseDatabase.getInstance().getReference().updateChildren(chatSync);
            return;
        } else {
            FirebaseDatabase.getInstance()
                    .getReference("channel-messages")
                    .child(chatRoom1)
                    .child("people-color")
                    .child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userColor = dataSnapshot.getValue(String.class);
                        Message.this.setColor(userColor);

                    } else {
                        Message.this.setColor("#2196F3");
                    }
                    FirebaseDatabase.getInstance().getReference().updateChildren(chatSync);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Message.this.setColor("#2196F3");
                    FirebaseDatabase.getInstance().getReference().updateChildren(chatSync);
                }
            });
        }

    }


    protected void performSaveMessage(String imageUrl, final Message message, final DatabaseReference databaseReference, String chatRoomId, FirebaseUser mFirebaseUser) {
        message.getAttachments().put("IMAGE", imageUrl);
        if (mFirebaseUser.getPhoneNumber() != null)
            message.setPhone(mFirebaseUser.getPhoneNumber());
        if (!message.getColor().isEmpty()) {
            databaseReference.setValue(message).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FA", e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            }).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            });
        } else {
            FirebaseDatabase.getInstance()
                    .getReference("channel-messages")
                    .child(chatRoomId)
                    .child("people-color")
                    .child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userColor = dataSnapshot.getValue(String.class);
                        message.setColor(userColor);

                    } else {
                        message.setColor("#2196F3");
                    }
                    databaseReference.setValue(message);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    message.setColor("#2196F3");
                    databaseReference.setValue(message);
                }
            });

        }
    }

    protected void performSaveMessage(String imageUrl, final Message message, String chatRoom1, String chatRoom2, FirebaseUser mFirebaseUser) {
        if (mFirebaseUser.getPhoneNumber() != null)
            message.setPhone(mFirebaseUser.getPhoneNumber());
        message.getAttachments().put("IMAGE", imageUrl);
        final Map<String, Object> chatSync = new HashMap<>();
        chatSync.put("channel-messages/" + chatRoom1 + "/messages/" + message.getMessageId(), message);
        chatSync.put("channel-messages/" + chatRoom2 + "/messages/" + message.getMessageId(), message);
        if (!message.getColor().isEmpty()) {
            FirebaseDatabase.getInstance().getReference().updateChildren(chatSync);
        } else {
            FirebaseDatabase.getInstance()
                    .getReference("channel-messages")
                    .child(chatRoom1)
                    .child("people-color")
                    .child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userColor = dataSnapshot.getValue(String.class);
                        message.setColor(userColor);

                    } else {
                        message.setColor("#2196F3");
                    }
                    FirebaseDatabase.getInstance().getReference().updateChildren(chatSync);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    message.setColor("#2196F3");
                    FirebaseDatabase.getInstance().getReference().updateChildren(chatSync);
                }
            });

        }
    }

    protected void showUploadNotification(int notId, int increment, boolean completed, Context context) {
        NotificationManager mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder mBuilder = new Notification.Builder(context);
        mBuilder.setContentTitle("Image Upload")
                .setContentText("Upload in progress")
                .setSmallIcon(R.drawable.ic_file_upload_black_24dp);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder.setChannelId(ApplicationSettingsUtil.IMAGE_UPLOADER_CHANNEL_ID);
        }
        if (completed) {
            mBuilder.setContentText("Upload complete")
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(notId, mBuilder.build());
            return;
        }

        mBuilder.setProgress(100, increment, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(notId, mBuilder.build());


    }


}

