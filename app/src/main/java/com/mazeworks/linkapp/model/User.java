package com.mazeworks.linkapp.model;

/**
 * Created by mambisiz on 9/3/17.
 */

public class User {
    private String mName;
    private String mProfileUrl;
    private String mUid;
    private String mEmail;
    private String mPhoneNumber;
    public User() {
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public User(String name, String profileUrl, String uid, String email) {
        mName = name;
        mProfileUrl = profileUrl;
        mUid = uid;
        mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getProfileUrl() {
        return mProfileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        mProfileUrl = profileUrl;
    }

    public String getUid() {
        return mUid;
    }

    public void setUid(String uid) {
        mUid = uid;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public static class UserContext{
        public static User USER_IN_CONTEXT = null;
    }
}


