package com.mazeworks.linkapp.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mazeworks.linkapp.model.Contact;
import com.mazeworks.linkapp.model.User;
import com.mukesh.countrypicker.Country;

import io.realm.Realm;

/**
 * Created by mambisiz on 9/24/17.
 */

public class ContactSyncService extends Service {
    private Cursor cursor;
    private Realm mRealm;
    private Country mCountry;
    private int counter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                getContacts();
            }
        }).start();

        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true,new ContactSyncObserver());
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mRealm = Realm.getDefaultInstance();
        mCountry = Country.getCountryFromSIM(getApplicationContext());
    }

    // left blank below constructor for this Contact observer example to work
// or if you want to make this work using Handler then change below registering  //line
    class ContactSyncObserver extends ContentObserver {

        public ContactSyncObserver() {
            super(null);
        }
        public ContactSyncObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
            Log.e("SYNC", "~~~~~~" + selfChange);
            // Override this method to listen to any changes
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    getContacts();
                }
            }).start();
            //On Contact add/delete this method is fired
        }
    }

    public void getContacts() {

        String phoneNumber = null;
        String email = null;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
        StringBuffer output;
        ContentResolver contentResolver = getContentResolver();
        cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                output = new StringBuffer();
                // Update the progress message

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    output.append("\n First Name:" + name);
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                        try {
                            String phoneE164 = "";
                            String phoneLocal = "";
                            Phonenumber.PhoneNumber phone = phoneUtil.parse(phoneNumber, mCountry.getCode());
                            if (phoneUtil.isValidNumber(phone)) {
                                phoneE164 = phoneUtil.format(phone, PhoneNumberUtil.PhoneNumberFormat.E164);
                                phoneLocal = phoneUtil.format(phone, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
                                final Contact contact = new Contact(name, phoneE164);
                                contact.setPhone(phoneLocal);

                                FirebaseDatabase.getInstance().getReference("users").orderByChild("phoneNumber").equalTo(contact.getFormattedPhoneNumber()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getChildrenCount() > 0) {

                                            for (DataSnapshot d :
                                                    dataSnapshot.getChildren()) {
                                                User con = d.getValue(User.class);
                                                contact.setImageUrl(con.getProfileUrl());
                                                contact.setUid(con.getUid());
                                                contact.setMessengerAvailable(true);

                                            }
                                        }
                                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm bgRealm) {
                                                bgRealm.copyToRealmOrUpdate(contact);
                                            }
                                        }, new Realm.Transaction.OnSuccess() {
                                            @Override
                                            public void onSuccess() {
                                                // Transaction was a success.
                                                Log.i("CONTACT_FOUND", contact.getName());
                                            }
                                        }, new Realm.Transaction.OnError() {
                                            @Override
                                            public void onError(Throwable error) {
                                                // Transaction failed and was automatically canceled.
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        } catch (NumberParseException e) {
                            System.err.println("NumberParseException was thrown: " + e.toString());
                        }
                        output.append("\n Phone number:" + phoneNumber);
                    }
                    phoneCursor.close();
                }
            }

        }
    }


}
