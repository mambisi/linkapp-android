package com.mazeworks.linkapp.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.Message;
import com.mazeworks.linkapp.util.ApplicationSettingsUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * Created by mambisiz on 9/18/17.
 */

public class ImageJobIntentService extends JobIntentService {

    final Handler mHandler = new Handler();
    public static void enqueueWork(Context context, Intent work) {
        int jid  = (int) new Date().getTime();
        enqueueWork(context, ImageJobIntentService.class, jid, work);
    }
    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String messageColor = intent.getExtras().getString("messageColor");
        Message message = new Message();
        message.setColor(messageColor);
        String imageUrl = intent.getExtras().getString("imageUrl");
        String chatRoomId = intent.getExtras().getString("chatRoomId");
        Uri imageUri = Uri.parse(imageUrl);
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] data = baos.toByteArray();
        message.getAttachmentMetadata().put("height", bitmap.getHeight());
        message.getAttachmentMetadata().put("width", bitmap.getWidth());
        message.getAttachmentMetadata().put("size", data.length);
        sendImageMessage(message, data, chatRoomId);

    }

    public void sendImageMessage(final Message message, byte[] imageBoas, final String chatRoomId) {
        Context context = getApplicationContext();
        final FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String defImage = context.getString(R.string.default_avatar_url);
        if (mFirebaseUser == null) {
            try {
                throw new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("channel-messages")
                .child(chatRoomId)
                .child("messages")
                .push();


        String pushId = databaseReference.getKey();
        message.setAuthorId(mFirebaseUser.getUid());
        message.setMessageId(pushId);


        message.setAuthorName(mFirebaseUser.getDisplayName());
        message.setAuthorProfileUrl(mFirebaseUser.getPhotoUrl() == null ? defImage : mFirebaseUser.getPhotoUrl().toString());
        message.setTimestamp(new Date().getTime());
        message.setBody(context.getString(R.string.default_image_message_body) + " " + mFirebaseUser.getDisplayName());

        UploadTask uploadTask = FirebaseStorage.getInstance()
                .getReference("imageAttachments/" + message.getMessageId() + "/" + UUID.randomUUID().toString())
                .putBytes(imageBoas);

        final int id = (int) new Date().getTime();



        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double i = taskSnapshot.getBytesTransferred();
                double j = taskSnapshot.getTotalByteCount();

                double k = j - i;
                double f = (i / j )*100L;
                int incr = (int) (f);

                showUploadNotification(id,incr,false);
            }
        });

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

        uploadTask.addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });

        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {



                showUploadNotification(id,0,true);


                Uri fileUri = taskSnapshot.getDownloadUrl();
                String ffs = fileUri.toString();
                performSaveMessage(ffs, message, databaseReference, chatRoomId, mFirebaseUser);
            }
        });
    }

    private void performSaveMessage(String imageUrl, final Message message, final DatabaseReference databaseReference, String chatRoomId, FirebaseUser mFirebaseUser) {
        message.getAttachments().put("IMAGE", imageUrl);

        if (!message.getColor().isEmpty()) {
            databaseReference.setValue(message).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FA", e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            }).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            });
        } else {
            FirebaseDatabase.getInstance()
                    .getReference("channel-messages")
                    .child(chatRoomId)
                    .child("people-color")
                    .child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String userColor = dataSnapshot.getValue(String.class);
                        message.setColor(userColor);

                    } else {
                        message.setColor("#2196F3");
                    }
                    databaseReference.setValue(message);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    message.setColor("#2196F3");
                    databaseReference.setValue(message);
                }
            });

        }
    }

    private void showUploadNotification(int notId,int increment,boolean completed) {
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder mBuilder = new Notification.Builder(this);
        mBuilder.setContentTitle("Image Upload")
                .setContentText("Upload in progress")
                .setSmallIcon(R.drawable.ic_file_upload_black_24dp);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder.setChannelId(ApplicationSettingsUtil.IMAGE_UPLOADER_CHANNEL_ID);
        }
        if(completed){
            mBuilder.setContentText("Upload complete")
                    // Removes the progress bar
                    .setProgress(0,0,false);
            mNotifyManager.notify(notId, mBuilder.build());
            return;
        }

        mBuilder.setProgress(100, increment, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(notId, mBuilder.build());


    }
}
