package com.mazeworks.linkapp.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mazeworks.linkapp.util.ApplicationSettingsUtil;

/**
 * Created by mambisiz on 9/12/17.
 */

public class MessagingIdInstance extends FirebaseInstanceIdService {
    private static final String TAG = "SERVICE";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        ApplicationSettingsUtil.sendRegistrationToServer(refreshedToken);
    }

}
