package com.mazeworks.linkapp.service;

import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.Seconds;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by mambisiz on 9/6/17.
 */

public class UserLastSeenUpdaterService extends JobService {
    private Timer timer = new Timer();

    @Override
    public boolean onStartJob(JobParameters job) {
        if (job.getTag().contentEquals("user-status-update")) {

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user != null){
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference myConnectionsRef = database.getReference("users/"+user.getUid()+"/connections");

                // stores the timestamp of my last disconnect (the last time I was seen online)

                final DatabaseReference lastOnlineRef = database.getReference("/users/"+user.getUid()+"/lastOnline");

                final DatabaseReference connectedRef = database.getReference(".info/connected");
                connectedRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        boolean connected = snapshot.getValue(Boolean.class);
                        if (connected) {
                            DatabaseReference con = myConnectionsRef.push();

                            // when this device disconnects, remove it
                            con.onDisconnect().removeValue();

                            // when I disconnect, update the last time I was seen online
                            lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);

                            // add this device to my connections list
                            // this value could contain info about the device or a timestamp too
                            con.setValue(Boolean.TRUE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        System.err.println("Listener was cancelled at .info/connected");
                    }
                });
                return true;
            }
            else {
                return false;
            }

        }
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

}