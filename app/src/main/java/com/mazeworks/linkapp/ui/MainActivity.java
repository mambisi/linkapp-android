package com.mazeworks.linkapp.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.ui.home.HomeActivity;
import com.mazeworks.linkapp.ui.login.EditUserProfileActivity;
import com.mazeworks.linkapp.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
        else if(user.getDisplayName()==null){
            startActivity(new Intent(this, EditUserProfileActivity.class));
            finish();
        }
        else if (user.getDisplayName().contentEquals("")) {
            startActivity(new Intent(this, EditUserProfileActivity.class));
            finish();
        } else if (!user.getDisplayName().contentEquals("")) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }

    }
}
