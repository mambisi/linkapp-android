package com.mazeworks.linkapp.ui.chat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.AudioMessage;
import com.mazeworks.linkapp.model.ChatRoom;

import static com.mazeworks.linkapp.ui.chat.ChatFragment.REQUEST_CODE;

/**
 * Created by mambisiz on 11/21/17.
 */

public class ChatActivity extends AppCompatActivity {
    private ChatRoom mChatRoom;
    private ChatFragment mChatFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Bundle bundle = getIntent().getExtras();
        String chatTitle = bundle.getString("title");
        mChatRoom = (ChatRoom) bundle.getSerializable("chatRoom");
        mChatFragment = ChatFragment.newInstance(chatTitle, mChatRoom);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mChatFragment).commitNow();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Great! User has recorded and saved the audio file
                final Uri audioUri = Uri.fromFile(mChatFragment.getFile());
                handleAudioRecorded(audioUri);

            } else if (resultCode == RESULT_CANCELED) {
                // Oops! User has canceled the recording
            }
        }
    }

    private void handleAudioRecorded(Uri audio) {
        AudioMessage audioMessage = new AudioMessage();
        if (mChatRoom.getReceiverChannelId() != null) {
            audioMessage.sendAudioMessage(this, audio, mChatRoom.getChannelId(), mChatRoom.getReceiverChannelId());
        } else {
            audioMessage.sendAudioMessage(this, audio, mChatRoom.getChannelId());
        }
    }
}
