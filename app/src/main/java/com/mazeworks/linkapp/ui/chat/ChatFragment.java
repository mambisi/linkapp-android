package com.mazeworks.linkapp.ui.chat;


import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import com.google.firebase.database.ValueEventListener;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.adapter.MessagesAdapter;
import com.mazeworks.linkapp.model.AudioMessage;
import com.mazeworks.linkapp.model.ChatRoom;
import com.mazeworks.linkapp.model.ImageMessage;
import com.mazeworks.linkapp.model.Message;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class ChatFragment extends Fragment implements MessagesAdapter.AdapterCallbacks {
    private static final String ARG_CHAT_ROOM = "chatRoom";
    private static final String ARG_CHAT_TITLE = "chatTitle";
    private static final int PLACE_PICKER_REQUEST = 4040;

    // TODO: Rename and change types of parameters
    private ChatRoom mChatRoom;
    private ImageButton mSendMessageButton;
    private AppCompatEditText mMessageBox;
    private ImageButton mCloseButton, mChatImage;
    private AppCompatImageButton mImageMessageButton, mAudioMessageButton, mLocationMessageButton,mSnapCaptureButton;
    private AppCompatTextView mChatTopic;
    private String mChatTitle;
    private RecyclerView mMessagesList;
    private static final int SELECT_PHOTO = 3001;

    private DatabaseReference mDatabaseReferenceMessages = FirebaseDatabase.getInstance().getReference("channel-messages");
    private FirebaseUser mFirebaseUser;
    private String mUserChatColor = "";
    public static final int REQUEST_CODE = 8888;
    public File file;
    public ChatFragment() {
        // Required empty public constructor
    }

    public static ChatFragment newInstance(String title, ChatRoom chatRoom) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CHAT_ROOM, chatRoom);
        args.putSerializable(ARG_CHAT_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (getArguments() != null) {
            mChatRoom = (ChatRoom) getArguments().getSerializable(ARG_CHAT_ROOM);
            mChatTitle = getArguments().getString(ARG_CHAT_TITLE);
            FirebaseDatabase.getInstance()
                    .getReference("channel-messages")
                    .child(mChatRoom.getChannelId())
                    .child("people-color")
                    .child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (mUserChatColor.isEmpty()) {
                        if (dataSnapshot.exists()) {
                            mUserChatColor = dataSnapshot.getValue(String.class);

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void pickImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {

                final Uri imageUri = data.getData();
                try {
                    handleImageSelected(imageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                Place place = PlacePicker.getPlace(getActivity(), data);
                String address = place.getAddress().toString();
                double latitude = place.getLatLng().latitude;
                double longitude = place.getLatLng().longitude;

                Message message = new Message();
                message.getAttachments().put("LOCATION", address);
                message.getAttachmentMetadata().put("latitude", latitude);
                message.getAttachmentMetadata().put("longitude", longitude);
                message.getAttachmentMetadata().put("placeId", place.getId());
                message.setColor(mUserChatColor);
                message.setBody(place.getName().toString());

                if (mChatRoom.getReceiverChannelId() != null) {
                    message.sendMessage(getContext(), mChatRoom.getChannelId(), mChatRoom.getReceiverChannelId());
                } else {
                    message.sendMessage(getContext(), mChatRoom.getChannelId());
                }
            }
        }else if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Great! User has recorded and saved the audio file
                final Uri audioUri = data.getData();
                handleAudioRecorded(audioUri);

            } else if (resultCode == RESULT_CANCELED) {
                // Oops! User has canceled the recording
            }
        }
    }

    private void pickLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleAudioRecorded(Uri audio){
        AudioMessage audioMessage = new AudioMessage();
        if (mChatRoom.getReceiverChannelId() != null) {
            audioMessage.sendAudioMessage(getActivity(),audio,mChatRoom.getChannelId(),mChatRoom.getReceiverChannelId());
        } else {
            audioMessage.sendAudioMessage(getActivity(),audio,mChatRoom.getChannelId());
        }
    }

    private void handleImageSelected(Uri imageUri) throws IOException {

        ImageMessage message = new ImageMessage();
        message.setColor(mUserChatColor);

        if (mChatRoom.getReceiverChannelId() != null) {
            message.sendImageMessage(imageUri.toString(), getContext(), mChatRoom.getChannelId(), mChatRoom.getReceiverChannelId());

        } else {
            message.sendImageMessage(imageUri.toString(), getContext(), mChatRoom.getChannelId());
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (mChatRoom.getReceiverChannelId() == null) {
            mChatTopic.setText("Quick Chat");
        } else {
            mChatTopic.setText(mChatTitle);

            FirebaseDatabase.getInstance().getReference("users").child(mChatRoom.getCreatorUID()).child("profileUrl").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String url = dataSnapshot.getValue(String.class);
                    if (url != null) {
                        RequestOptions requestOptions = new RequestOptions().circleCrop();
                        Glide.with(getActivity()).load(url).apply(requestOptions).into(mChatImage);
                        mChatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showChatProfile(mChatRoom.getCreatorUID());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        setUpViews();


    }

    private void setUpViews() {
        Query queryChatRoomMessages = mDatabaseReferenceMessages.child(mChatRoom.getChannelId()).child("messages").orderByChild("timestamp");
        queryChatRoomMessages.keepSynced(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        mMessagesList.setAdapter(new

                MessagesAdapter(queryChatRoomMessages, Message.class, getContext(), mChatRoom.

                getChannelId(), this));
        mMessagesList.setLayoutManager(linearLayoutManager);
        mCloseButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        mSendMessageButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                preformSendMessage();
                mMessageBox.clearComposingText();
                mMessageBox.getText().clear();
            }
        });


        mLocationMessageButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                PermissionListener permissionlistener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        pickLocation();

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }


                };

                new TedPermission(getContext())
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                        .check();

            }
        });
        mAudioMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAudioRecorder();
            }
        });
        mImageMessageButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });
    }

    private void showChatProfile(String userId) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("CHAT_PROFILE");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = ChatProfileFragment.newInstance(userId);
        newFragment.show(ft, "CHAT_PROFILE");


    }

    private void showAudioRecorder(){

        file = new File(getContext().getCacheDir(), "hello");
        int color = getResources().getColor(R.color.colorPrimaryDark);
        AndroidAudioRecorder.with(getActivity())
                // Required
                .setFilePath(file.getAbsolutePath())
                .setColor(color)
                .setRequestCode(REQUEST_CODE)

                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(true)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
    }


    private void preformSendMessage() {

        if (mMessageBox.getText().length() > 0) {

            Message message = new Message(mMessageBox.getText().toString());
            message.setColor(mUserChatColor);

            if (mChatRoom.getReceiverChannelId() != null) {
                message.sendMessage(getContext(), mChatRoom.getChannelId(), mChatRoom.getReceiverChannelId());
            } else
                message.sendMessage(getContext(), mChatRoom.getChannelId());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        mSendMessageButton = view.findViewById(R.id.sendMessageButton);
        mMessageBox = view.findViewById(R.id.messageBox);
        mCloseButton = view.findViewById(R.id.closeButton);
        mChatTopic = view.findViewById(R.id.chatTopic);
        mMessagesList = view.findViewById(R.id.messagesList);
        mImageMessageButton = view.findViewById(R.id.imageMessageButton);
        mAudioMessageButton = view.findViewById(R.id.audioMessageButton);
        mLocationMessageButton = view.findViewById(R.id.locationMessageButton);
        mChatImage = view.findViewById(R.id.chatImage);
        mSnapCaptureButton = view.findViewById(R.id.snapCapture);
        mSnapCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SnapCaptureActivity.class);
                startActivityForResult(intent, 3459);
            }
        });
        return view;
    }

    @Override
    public void onItemAdd() {
        if (mMessagesList.getAdapter() != null) {
            mMessagesList.smoothScrollToPosition(mMessagesList.getAdapter().getItemCount() - 1);
        }

    }

    public File getFile() {
        return file;
    }
}
