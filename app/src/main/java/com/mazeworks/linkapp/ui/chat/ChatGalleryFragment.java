package com.mazeworks.linkapp.ui.chat;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.adapter.GalleryViewAdapter;
import com.mazeworks.linkapp.model.GalleryItem;
import com.mazeworks.linkapp.util.DepthPageTransformer;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatGalleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatGalleryFragment extends DialogFragment {
    private String mChannelId;
    private String mMessageId;

    private static String ARG_CHANNEL_ID = "channelId";
    private static String ARG_MESSAGE_ID = "messageId";
    private RealmResults<GalleryItem> mGalleryItems;
    private Realm mRealm;

    public ChatGalleryFragment() {
        // Required empty public constructor
    }


    public static ChatGalleryFragment newInstance(String channelId, String messageId) {
        ChatGalleryFragment fragment = new ChatGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_CHANNEL_ID, channelId);
        bundle.putString(ARG_MESSAGE_ID, messageId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
        if (getArguments() != null) {
            mChannelId = getArguments().getString(ARG_CHANNEL_ID);
            mMessageId = getArguments().getString(ARG_MESSAGE_ID);

            RealmQuery<GalleryItem> query = mRealm.where(GalleryItem.class);
            query.equalTo(ARG_CHANNEL_ID, mChannelId);
            mGalleryItems = query.findAll();
            System.out.println(mGalleryItems);
        }
        setStyle(STYLE_NO_FRAME, R.style.DialogSlideAnim);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat_gallery, container, false);
        final ViewPager galleryPager = view.findViewById(R.id.galleryViewPager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            galleryPager.setTransitionName(mMessageId);
        }
        galleryPager.setPageTransformer(true, new DepthPageTransformer());

        galleryPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    galleryPager.setTransitionName(mGalleryItems.get(position).getMessageId());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        galleryPager.setAdapter(new GalleryViewAdapter(getChildFragmentManager(), mGalleryItems, getContext()));
        for (int i = 0; i <= mGalleryItems.size() - 1; i++) {
            GalleryItem galleryItem = mGalleryItems.get(i);
            String mssId = galleryItem.getMessageId();
            if (galleryItem.getMessageId().contentEquals(mMessageId)) {
                galleryPager.setCurrentItem(i, false);
                break;
            }
        }

        return view;
    }


}
