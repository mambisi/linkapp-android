package com.mazeworks.linkapp.ui.chat;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.User;


public class ChatProfileFragment extends DialogFragment {

    private static final String ARG_USER = "user";
    private AppCompatImageView mProfilePicture;
    private AppCompatTextView mContactPhoneNumber;
    private String mUserId;
    private Toolbar mToolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    public ChatProfileFragment() {
        // Required empty public constructor
    }

    public static ChatProfileFragment newInstance(String userId) {
        ChatProfileFragment fragment = new ChatProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(ARG_USER);
        }
        setStyle(STYLE_NORMAL,R.style.AppThemeActivitySettings);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_chat_profile, container, false);
        mProfilePicture = view.findViewById(R.id.profile_image);
        mContactPhoneNumber = view.findViewById(R.id.contactPhoneNumber);
        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(drawable);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users").child(mUserId);
        databaseReference.keepSynced(true);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user != null){
                    mToolbar.setTitle(user.getName());
                    mContactPhoneNumber.setText(user.getPhoneNumber());
                    Glide.with(getActivity()).load(user.getProfileUrl()).into(mProfilePicture);
                }
                else {
                    dismiss();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

}
