package com.mazeworks.linkapp.ui.chat;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.mazeworks.linkapp.R;

public class PhotoViewerFragment extends Fragment {

    private static final String ARG_IMAGE_URL = "imageURl";
    private String imageUrl;
    public PhotoViewerFragment() {
        // Required empty public constructor
    }

    public static PhotoViewerFragment newInstance(String imageUrl ) {
        PhotoViewerFragment fragment = new PhotoViewerFragment();
        Bundle bundle =  new Bundle();
        bundle.putString(ARG_IMAGE_URL,imageUrl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            imageUrl = getArguments().getString(ARG_IMAGE_URL);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo_viewer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        PhotoView photoView = view.findViewById(R.id.photoView);
        Glide.with(this).load(imageUrl).into(photoView);
    }
}
