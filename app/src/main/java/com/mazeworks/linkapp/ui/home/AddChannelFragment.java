package com.mazeworks.linkapp.ui.home;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.ChatRoom;
import com.mazeworks.linkapp.model.AddChatDialogClosedEvent;
import com.mazeworks.linkapp.model.CloseCameraFragmentShowMessages;

import org.greenrobot.eventbus.EventBus;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddChannelFragment extends BottomSheetDialogFragment {

    private static final String ARG_CHANNEL= "channel";

    private ChatRoom mChatRoom;
    private CircleImageView mChatImage;
    private AppCompatTextView mChatDesc,mChatTopic;
    private AppCompatButton mCreateChatButton;
    private DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference("user-channels");
    public AddChannelFragment() {
        // Required empty public constructor
    }

    public static AddChannelFragment newInstance(ChatRoom param1) {
        AddChannelFragment fragment = new AddChannelFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CHANNEL, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mChatRoom = (ChatRoom) getArguments().getSerializable(ARG_CHANNEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_channel, container, false);
    }

    @Override
    public void dismiss() {

        super.dismiss();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().post(new AddChatDialogClosedEvent("Hello everyone!"));

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mChatImage = view.findViewById(R.id.chatImage);
        mChatDesc = view.findViewById(R.id.chatDesc);
        mChatTopic = view.findViewById(R.id.chatTopic);
        mCreateChatButton = view.findViewById(R.id.createChatButton);
        mCreateChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCancelable(false);
                mDatabaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(mChatRoom.getChannelId()).setValue(mChatRoom).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        dismiss();
                        EventBus.getDefault().post(new CloseCameraFragmentShowMessages(mChatRoom));
                    }
                });
            }
        });
        Glide.with(getContext()).load(mChatRoom.getImageUrl()).into(mChatImage);
        mChatDesc.setText(mChatRoom.getDesc());
        mChatTopic.setText(mChatRoom.getTopic());
    }
}
