package com.mazeworks.linkapp.ui.home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.ChatRoom;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.ocpsoft.prettytime.units.Day;

import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatChannelItemMenuDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatChannelItemMenuDialogFragment extends BottomSheetDialogFragment {
    private DatabaseReference mDatabaseReference;
    private ChatRoom mChatRoom;
    private static String ARG_CHAT_ROOM = "chatRoom";
    private AppCompatTextView mChatTopic, mChatDesc, mChatStatus;
    private CircleImageView mChatImage;
    private AppCompatButton mDeleteBtn, mMemberBtn, mShareLinkBtn, mShareQrBtn;


    public ChatChannelItemMenuDialogFragment() {
        // Required empty public constructor
    }

    public static ChatChannelItemMenuDialogFragment newInstance(ChatRoom chatRoom) {
        ChatChannelItemMenuDialogFragment fragment = new ChatChannelItemMenuDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CHAT_ROOM, chatRoom);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mFirebaseUser != null)
            mDatabaseReference = FirebaseDatabase.getInstance().getReference("user-channels").child(mFirebaseUser.getUid());
        if (getArguments() != null) {
            mChatRoom = (ChatRoom) getArguments().getSerializable(ARG_CHAT_ROOM);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Glide.with(view).load(mChatRoom.getImageUrl()).into(mChatImage);
        mChatTopic.setText(mChatRoom.getTopic());
        if (mChatRoom.getReceiverChannelId() != null) {
            mChatDesc.setText("Messenger");
            mChatStatus.setText("Long Time");
        } else {
            if (mChatRoom.getDesc() != null)
                mChatDesc.setText(mChatRoom.getDesc().toLowerCase());
            mChatStatus.setText(showTimePassedDesc(mChatRoom));
        }

    }

    private String showTimePassedDesc(ChatRoom chatRoom) {
        DateTime dateTimeNow = new DateTime();
        DateTime chatStartDateTime = new DateTime(chatRoom.getStartTimestamp());
        DateTime chatEndDataTime = new DateTime(chatRoom.getEndTimestamp());
        int daysPassed = Days.daysBetween(dateTimeNow, chatStartDateTime).getDays();
        switch (daysPassed) {
            case -1:
                return "Chat ended yesterday at " + chatEndDataTime.toString(DateTimeFormat.forPattern("h:mm a"));
            case 0:
                return "Chat will end Today at " + chatEndDataTime.toString(DateTimeFormat.forPattern("h:mm a"));

            case 1:
                return "Chat will end Tomorrow at " + chatEndDataTime.toString(DateTimeFormat.forPattern("h:mm a"));

        }

        if (daysPassed < -1) {
            return "Chat ended " + Math.abs(daysPassed) + " days ago";
        } else if (daysPassed > 1) {
            return "Chat will end on " + chatEndDataTime.dayOfWeek().getAsText();
        }

        return "Calculating ...";

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View itemView = inflater.inflate(R.layout.fragment_chat_channel_menu, container, false);
        mChatImage = itemView.findViewById(R.id.chatImage);
        mChatTopic = itemView.findViewById(R.id.chatTopic);
        mChatDesc = itemView.findViewById(R.id.chatDesc);
        mChatStatus = itemView.findViewById(R.id.chatStatus);
        mDeleteBtn = itemView.findViewById(R.id.bottom_menu_delete);
        mMemberBtn = itemView.findViewById(R.id.bottom_menu_members);
        mShareLinkBtn = itemView.findViewById(R.id.bottom_menu_share_link);
        mShareQrBtn = itemView.findViewById(R.id.bottom_menu_share_qr);

        if (mChatRoom.getReceiverChannelId() != null) {
            mMemberBtn.setEnabled(false);
            mShareQrBtn.setEnabled(false);
            mShareLinkBtn.setEnabled(false);
        }

        addMenuListListener();
        return itemView;
    }

    private void addMenuListListener() {
        AppCompatButton[] mMenuItems = {mDeleteBtn, mMemberBtn, mShareLinkBtn, mShareQrBtn};
        for (AppCompatButton menuItem :
                mMenuItems) {
            menuItem.setOnClickListener(new MenuItemSelected(mChatRoom));
        }
    }


    private class MenuItemSelected implements View.OnClickListener {
        private ChatRoom mChatRoom;

        public MenuItemSelected(ChatRoom chatRoom) {
            mChatRoom = chatRoom;
        }

        @Override
        public void onClick(View view) {
            int menuId = view.getId();
            switch (menuId) {
                case R.id.bottom_menu_share_qr:
                    showQRCodeDialog(mChatRoom.getChannelId(), mChatRoom.getTopic());
                    break;
                case R.id.bottom_menu_share_link:

                    break;
                case R.id.bottom_menu_members:
                    break;
                case R.id.bottom_menu_delete:
                    mDatabaseReference.child(mChatRoom.getChannelId()).removeValue();
                    dismissAllowingStateLoss();
                    break;
            }

        }
    }

    private void showQRCodeDialog(String chatRoomId, String chatTitle) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("CREATE_QR");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = QRCodeGeneratorFragment.newInstance(chatRoomId, chatTitle);
        newFragment.show(ft, "CREATE_QR");
    }
}
