package com.mazeworks.linkapp.ui.home;



import android.app.ProgressDialog;

import android.content.Context;
import android.database.Cursor;

import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.adapter.ContactListAdapter;
import com.mazeworks.linkapp.model.Contact;
import com.mukesh.countrypicker.Country;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class ContactListFragment extends DialogFragment {


    private OnFragmentInteractionListener mListener;

    private ArrayList<Contact> contactList;
    private RecyclerView mContactListView;
    private Realm mRealm;

    public ContactListFragment() {
        // Required empty public constructor
    }


    public static ContactListFragment newInstance() {
        ContactListFragment fragment = new ContactListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL,R.style.AppThemeActivitySettings);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        mContactListView = view.findViewById(R.id.list);
        mRealm = Realm.getDefaultInstance();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getContacts() {
        RealmResults<Contact> contacts = mRealm.where(Contact.class).findAllSortedAsync("name");
        contactList = new ArrayList<>();
        contacts.addChangeListener(new RealmChangeListener<RealmResults<Contact>>() {
            @Override
            public void onChange(@NonNull RealmResults<Contact> contactRealmResults) {
                contactList.addAll(contactRealmResults);
                mContactListView.setAdapter(new ContactListAdapter(getContext(),contactList,mListener));
                mContactListView.setLayoutManager(new LinearLayoutManager(getContext()));
            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        getContacts();
    }

    public interface OnFragmentInteractionListener {
        void onContactSelected(Contact contact);
    }
}
