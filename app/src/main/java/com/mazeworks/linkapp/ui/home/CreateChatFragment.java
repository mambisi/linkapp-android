package com.mazeworks.linkapp.ui.home;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.ChatRoom;
import com.mazeworks.linkapp.model.ColorPalette;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class CreateChatFragment extends DialogFragment {

    private ImageButton mPrevDuration, mNextDuration, mPrevMaxPeople, mNextMaxPeople;
    private AppCompatTextView mDurationTV, mMaxPeopleTV;
    private AppCompatEditText mTopicET;
    private AppCompatButton createBtn;
    private DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
    private static Integer mMinDurationCounter = 1;
    private static Integer mDefaultPeopleCounter = 5;
    private ProgressDialog mProgressDialog;
    private FirebaseUser mUser;
    private CreateChatInteraction mInteraction;

    public CreateChatFragment() {
        // Required empty public constructor
    }


    public static CreateChatFragment newInstance() {
        CreateChatFragment fragment = new CreateChatFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.AppTheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {



        createBtn = view.findViewById(R.id.createChatButton);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createChat();
            }
        });

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mUser == null) {
            try {
                throw new Exception("FIREBASE USER NULL");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Creating Chat");


        mPrevDuration = view.findViewById(R.id.prevDuration);
        mPrevDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prevDuration();
            }
        });

        mNextDuration = view.findViewById(R.id.nextDuration);
        mNextDuration.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                doubleNextDuration();
                return true;
            }
        });
        mNextDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextDuration();
            }
        });

        mPrevMaxPeople = view.findViewById(R.id.prevMP);
        mPrevMaxPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prevMP();
            }
        });

        mNextMaxPeople = view.findViewById(R.id.nextMP);
        mNextMaxPeople.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                doubleNextMP();
                return true;
            }
        });
        mNextMaxPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextMP();
            }
        });

        mDurationTV = view.findViewById(R.id.durationTV);
        mMaxPeopleTV = view.findViewById(R.id.maxPeopleTV);
        mTopicET = view.findViewById(R.id.chatTopic);


    }

    private void prevDuration() {
        if (mMinDurationCounter > 1) {
            mMinDurationCounter--;
            String s = "" + mMinDurationCounter + " hr";
            mDurationTV.setText(s);
        }
    }

    private void nextDuration() {
        if (mMinDurationCounter < 24) {
            mMinDurationCounter++;
            String s = "" + mMinDurationCounter + " hr";
            mDurationTV.setText(s);
        }
    }

    private void doubleNextDuration() {
        int holder = mMinDurationCounter;
        mMinDurationCounter *= 2;
        if (mMinDurationCounter < 24) {
            String s = "" + mMinDurationCounter + " hr";
            mDurationTV.setText(s);
        } else {
            mMinDurationCounter = holder;
        }
    }


    private void prevMP() {
        if (mDefaultPeopleCounter > 2) {
            mDefaultPeopleCounter--;
            String s = "" + mDefaultPeopleCounter;
            mMaxPeopleTV.setText(s);
        }
    }

    private void nextMP() {
        if (mDefaultPeopleCounter < 300) {
            mDefaultPeopleCounter++;
            String s = "" + mDefaultPeopleCounter;
            mMaxPeopleTV.setText(s);
        }
    }

    private void doubleNextMP() {

        int holder = mDefaultPeopleCounter;
        mDefaultPeopleCounter *= 2;
        if (mDefaultPeopleCounter < 300) {

            String s = "" + mDefaultPeopleCounter;
            mMaxPeopleTV.setText(s);
        } else {
            mDefaultPeopleCounter = holder;
        }
    }

    private void createChat() {


        mProgressDialog.show();
        if (mTopicET.getText().length() > 5) {
            ColorPalette colorPalette =  ColorPalette.getRandomColor();
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.HOUR_OF_DAY, mMinDurationCounter);
            String defaultIconUrl = getString(R.string.default_channel_icon_url);
            final ChatRoom chatRoom = new ChatRoom();
            chatRoom.setCreatorUID(mUser.getUid());
            chatRoom.setCurrentMessageTimestamp(date.getTime());
            chatRoom.setDuration(mMinDurationCounter);
            chatRoom.setMaxPeople(mDefaultPeopleCounter);
            if (mUser.getPhotoUrl() != null)
                chatRoom.setImageUrl(mUser.getPhotoUrl().toString());
            else
                chatRoom.setImageUrl(defaultIconUrl);
            chatRoom.setTopic(mTopicET.getText().toString());
            chatRoom.setStartTimestamp(date.getTime());
            chatRoom.setEndTimestamp(calendar.getTimeInMillis());
            chatRoom.setDesc("created by "+ mUser.getDisplayName() );

            chatRoom.getColorPalette().put("color100",colorPalette.getColor100());
            chatRoom.getColorPalette().put("color700",colorPalette.getColor700());

            DatabaseReference reference = mDatabaseReference.push();
            chatRoom.setChannelId(reference.getKey());

            Map<String, Object> chatRoomMap = new HashMap<>();
            chatRoomMap.put("channels/" + chatRoom.getChannelId(), chatRoom);
            chatRoomMap.put("user-channels/" + mUser.getUid() + "/" + chatRoom.getChannelId(), chatRoom);

            mDatabaseReference.updateChildren(chatRoomMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    dismiss();
                    mInteraction.onChatCreated(chatRoom.getChannelId(), chatRoom.getTopic());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (getView() != null)
                        Snackbar.make(getView(), e.getMessage(), BaseTransientBottomBar.LENGTH_LONG).show();
                }
            });

        }



    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CreateChatInteraction){
            mInteraction = (CreateChatInteraction) context;
        }
        else {
            try {
                throw new Exception("CONTEXT ERROR");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInteraction = null;
    }

    public interface CreateChatInteraction{
        void onChatCreated(String chatRoomId, String chatTitle);
    }


}
