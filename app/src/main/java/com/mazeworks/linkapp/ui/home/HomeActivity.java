package com.mazeworks.linkapp.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.ChatRoom;
import com.mazeworks.linkapp.model.CloseCameraFragmentShowMessages;
import com.mazeworks.linkapp.model.Contact;
import com.mazeworks.linkapp.service.ContactSyncService;
import com.mazeworks.linkapp.service.UserLastSeenUpdaterService;
import com.mazeworks.linkapp.util.ApplicationSettingsUtil;
import com.mukesh.countrypicker.Country;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

public class HomeActivity extends AppCompatActivity
        implements CreateChatFragment.CreateChatInteraction,
        QRCameraFragment.OnFragmentInteractionListener,
        GoogleApiClient.OnConnectionFailedListener, MessageListFragment.RecyclerViewInteraction, ContactListFragment.OnFragmentInteractionListener {

    private FrameLayout mLayout;
    private HomeContentFragment mHomeContentFragment = HomeContentFragment.newInstance();
    private QRCameraFragment mQRCameraFragment;
    private FloatingActionButton actionButton;
    private String barcode;
    private PlaceDetectionClient mPlaceDetectionClient;
    private GoogleApiClient mGoogleApiClient;
    private Handler updateBarHandler;
    private Cursor cursor;
    private int counter;
    private Realm mRealm;
    private Country mCountry;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startStatusUpdater();
        setContentView(R.layout.activity_home);

        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        mRealm = Realm.getDefaultInstance();
        mCountry = Country.getCountryFromSIM(getApplicationContext());
        updateBarHandler = new Handler();
        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        mLayout = findViewById(R.id.fragmentContainer);

        actionButton = findViewById(R.id.qrCameraBtn);
        actionButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                showQrCameraFragment();

                return true;
            }
        });

        showPermissions();
        showHomeContentFragment();
        handleLink();


        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        ApplicationSettingsUtil.sendRegistrationToServer(refreshedToken);


    }

    private void showPermissions() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                mQRCameraFragment = QRCameraFragment.newInstance(actionButton.getX(), actionButton.getY(), actionButton.getWidth(), actionButton.getHeight());

                    Intent startIntent = new Intent(HomeActivity.this, ContactSyncService.class);
                    startService(startIntent);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                mQRCameraFragment = null;
            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_CONTACTS)
                .check();
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            try {
                URL url = new URL("http://www.google.com/");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setRequestProperty("User-Agent", "test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1000); // mTimeout is in seconds
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                Log.i("warning", "Error checking internet connection", e);
                return false;
            }
        }

        return false;

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleLink();
    }

    private void handleLink() {
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();

        if (appLinkData != null) {
            String channelId = appLinkData.getQueryParameter("channelId");
            System.out.println("ChannelId: " + channelId);
            if (channelId != null) {
                showAddChannelDialog(channelId);
            }
        }
    }

    private void showAddChannelDialog(String chatId) {
        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference("channels");
        mDatabaseReference.child(chatId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    ChatRoom chatRoom = dataSnapshot.getValue(ChatRoom.class);
                    AddChannelFragment.newInstance(chatRoom).show(getSupportFragmentManager(), "ADD_CHANNEL");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void showHomeContentFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(mLayout.getId(), mHomeContentFragment).commitNowAllowingStateLoss();
    }

    private void showQrCameraFragment() {
        //TODO setup QrCode Scanner Fragment

        //getSupportFragmentManager().findFragmentByTag()


        if (mQRCameraFragment != null) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(mLayout.getId(), mQRCameraFragment)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
            actionButton.hide();
        } else {

            if (mHomeContentFragment.getView() != null)
                Snackbar.make(mHomeContentFragment.getView(), "Enable Camera and Storage Permissions to use this feature", BaseTransientBottomBar.LENGTH_LONG).show();
            else
                Toast.makeText(this, "Enable Camera Permissions to use this feature", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onChatCreated(String chatRoomId, String chatTitle) {
        mHomeContentFragment.pushToMessages();
        showQRCodeDialog(chatRoomId, chatTitle);

    }

    private void showQRCodeDialog(String chatRoomId, String chatTitle) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("CREATE_QR");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = QRCodeGeneratorFragment.newInstance(chatRoomId, chatTitle);
        newFragment.show(ft, "CREATE_QR");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));
        dispatcher.cancel("user-status-update");
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowMessagesDialog(CloseCameraFragmentShowMessages event) {
        if (getSupportFragmentManager().popBackStackImmediate()) {
            actionButton.show();
        } else {
            showHomeContentFragment();
        }
        mHomeContentFragment.pushToMessages();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        actionButton.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void startStatusUpdater() {
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));

        Job myJob = dispatcher.newJobBuilder()
                .setService(UserLastSeenUpdaterService.class) // the JobService that will be called
                .setTag("user-status-update")// uniquely identifies the job
                .setTrigger(Trigger.executionWindow(0, 0))
                .setReplaceCurrent(true)
                .build();

        dispatcher.mustSchedule(myJob);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void hideFAB() {
        actionButton.hide();
    }

    @Override
    public void showFAB() {
        actionButton.show();
    }

    @Override
    public void showContacts() {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("CONTACT_DIALOG");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = ContactListFragment.newInstance();
        newFragment.show(ft, "CONTACT_DIALOG");
    }

    @Override
    public void onContactSelected(Contact contact) {
        DialogFragment dialogFragment;
        if (getSupportFragmentManager().findFragmentByTag("CONTACT_DIALOG") != null &&
                getSupportFragmentManager().findFragmentByTag("CONTACT_DIALOG") instanceof ContactListFragment) {
            ((ContactListFragment) getSupportFragmentManager().findFragmentByTag("CONTACT_DIALOG")).dismiss();
            createChatRoom(mFirebaseUser,contact.getUid(),contact.getName());
        }
    }

    private void createChatRoom(FirebaseUser mUser , String  receiverId,String name){
        final ChatRoom chatRoom1 = new ChatRoom();
        String defaultIconUrl = getString(R.string.default_channel_icon_url);
        chatRoom1.setMaxPeople(2);
        chatRoom1.setImageUrl(defaultIconUrl);
        chatRoom1.setTopic(name);
        chatRoom1.setCreatorUID(receiverId);
        chatRoom1.setChannelId(mUser.getUid() +"_from_to_"+receiverId);
        chatRoom1.setStartTimestamp(DateTime.now().toDate().getTime());
        chatRoom1.setEndTimestamp(DateTime.now().plusYears(10).toDate().getTime());

        final ChatRoom chatRoom2 = new ChatRoom();
        chatRoom2.setMaxPeople(2);
        if (mUser.getPhotoUrl() != null)
            chatRoom2.setImageUrl(mUser.getPhotoUrl().toString());
        else
            chatRoom2.setImageUrl(defaultIconUrl);
        chatRoom2.setChannelId(receiverId+"_from_to_"+mUser.getUid());
        chatRoom2.setTopic(mUser.getDisplayName());
        chatRoom2.setCreatorUID(mUser.getUid());
        chatRoom2.setStartTimestamp(DateTime.now().getMillis());
        chatRoom2.setEndTimestamp(DateTime.now().plusYears(10).getMillis());

        chatRoom1.setReceiverChannelId(chatRoom2.getChannelId());
        chatRoom2.setReceiverChannelId(chatRoom1.getChannelId());

        Map<String, Object> chatRoomMap = new HashMap<>();
        chatRoomMap.put("channels/" + chatRoom1.getChannelId(), chatRoom1);
        chatRoomMap.put("user-channels/" + mUser.getUid() + "/" + chatRoom1.getChannelId(), chatRoom1);
        chatRoomMap.put("channels/" + chatRoom2.getChannelId(), chatRoom2);
        chatRoomMap.put("user-channels/" + receiverId + "/" + chatRoom2.getChannelId(), chatRoom2);

        mDatabaseReference.updateChildren(chatRoomMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }
}
