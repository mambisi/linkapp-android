package com.mazeworks.linkapp.ui.home;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.adapter.HomeFragmentViewPagerAdapter;


public class HomeContentFragment extends Fragment {

    private ViewPager mViewPager;
    public HomeContentFragment() {
        // Required empty public constructor
    }
    public static HomeContentFragment newInstance() {
        HomeContentFragment fragment = new HomeContentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        mViewPager = view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new HomeFragmentViewPagerAdapter(getChildFragmentManager()));

        tabLayout.setupWithViewPager(mViewPager);

        setTabColors(tabLayout);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_content, container, false);
    }

    public void pushToMessages(){
        mViewPager.setCurrentItem(1, true);
    }

    private void setTabColors(TabLayout tabLayout) {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_tab_profile);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_tab_msg);
        int tabIconColor = ContextCompat.getColor(getContext(), R.color.tab_selected);
        tabLayout.getTabAt(0).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        tabIconColor = ContextCompat.getColor(getContext(), R.color.tab_unselected);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int tabIconColor = ContextCompat.getColor(getContext(), R.color.tab_selected);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int tabIconColor = ContextCompat.getColor(getContext(), R.color.tab_unselected);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
    }

}
