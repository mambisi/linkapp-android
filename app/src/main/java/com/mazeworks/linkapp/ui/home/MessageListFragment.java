package com.mazeworks.linkapp.ui.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.adapter.MessageChannelsListAdapter;
import com.mazeworks.linkapp.model.ChatRoom;

public class MessageListFragment extends Fragment {
    private RecyclerView mMessagesChannelList;
    private DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference("user-channels");
    private RecyclerViewInteraction mListener;
    private AppCompatButton mQuickChatButton,mFriendChatButton,mArchiveButton;
    public static MessageListFragment newInstance() {
        MessageListFragment fragment = new MessageListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mMessagesChannelList = view.findViewById(R.id.messageChannels);
        mQuickChatButton = view.findViewById(R.id.quickChatButton);
        mArchiveButton = view.findViewById(R.id.archiveButton);
        mFriendChatButton = view.findViewById(R.id.friendChatButton);
        mFriendChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showContacts();
            }
        });
        mQuickChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null){
            try {
                throw  new Exception("NO USER");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LinearLayoutManager  linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        Query userChannelsQuery = mDatabaseReference.child(user.getUid()).orderByChild("currentMessageTimestamp");
        userChannelsQuery.keepSynced(true);
        mMessagesChannelList.setAdapter(new MessageChannelsListAdapter(userChannelsQuery, ChatRoom.class, getContext()));
        mMessagesChannelList.setLayoutManager(linearLayoutManager);
        mMessagesChannelList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    // Do something
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    // Do something
                } else {
                    // Do something
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                    mListener.hideFAB();
                } else {
                    // Scrolling down
                    mListener.showFAB();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message_list, container, false);
    }


    interface RecyclerViewInteraction{
        void hideFAB();
        void showFAB();
        void showContacts();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof  RecyclerViewInteraction){
            mListener = (RecyclerViewInteraction) context;
        }
    }

    void showDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("CREATE_CHAT");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = CreateChatFragment.newInstance();
        newFragment.show(ft, "CREATE_CHAT");
    }
    @Override
    public void onDetach() {
        mListener.showFAB();
        super.onDetach();
        mListener = null;
    }

    public String getTitle(){
        return "Messages";
    }
}
