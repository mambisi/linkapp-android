package com.mazeworks.linkapp.ui.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.ChatRoom;
import com.mazeworks.linkapp.model.AddChatDialogClosedEvent;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Hashtable;

import static android.app.Activity.RESULT_OK;


public class QRCameraFragment extends Fragment implements QRCodeReaderView.OnQRCodeReadListener{

    private OnFragmentInteractionListener mListener;
    private QRCodeReaderView qrCodeReaderView;
    private String barcode;
    private AppCompatButton mPhotoLibrary, mAddLink;
    private DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference("channels");
    private FrameLayout mOverlayView;
    private double fabX;
    private double fabY;
    private static double fabHeight;
    private static double fabWidth;
    private LinearLayoutCompat revealLayout;
    private final int SELECT_PHOTO = 10053;

    public QRCameraFragment() {
        // Required empty public constructor
    }

    public static QRCameraFragment newInstance() {
        QRCameraFragment fragment = new QRCameraFragment();
        return fragment;
    }

    public static QRCameraFragment newInstance(double fabX, double fabY, double fabWidth, double fabHeight) {
        QRCameraFragment fragment = new QRCameraFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("fabX", fabX);
        bundle.putDouble("fabY", fabY);
        bundle.putDouble("fabWidth", fabWidth);
        bundle.putDouble("fabHeight", fabHeight);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fabY = getArguments().getDouble("fabY");
            fabX = getArguments().getDouble("fabX");
            fabWidth = getArguments().getDouble("fabWidth");
            fabHeight = getArguments().getDouble("fabHeight");
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void enterReveal() {
        // previously invisible view
        final View myView = getView().findViewById(R.id.revealLayout);

        // get the center for the clipping circle
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / 2;
        //myView.setVisibility(View.VISIBLE);
        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
        anim.setDuration(400);
        //anim.setStartDelay(200);
        anim.setInterpolator(new DecelerateInterpolator());
        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qrcamera, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        qrCodeReaderView = view.findViewById(R.id.qrdecoderview);
        mPhotoLibrary = view.findViewById(R.id.photoLibrary);
        mAddLink = view.findViewById(R.id.addLink);
        mOverlayView = view.findViewById(R.id.view_overlay);
        revealLayout = view.findViewById(R.id.revealLayout);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            revealLayout.setVisibility(View.VISIBLE);
        } else {
            revealLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        enterReveal();
                    }
                }
            });
        }
        mPhotoLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        qrCodeReaderView.setOnQRCodeReadListener(this);

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);
        qrCodeReaderView.setBackCamera();
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        if (text != null) {

            Uri hUri = Uri.parse(text);
            if (text.startsWith("https://linkappmessenger.com/?channelId=")) {
                mOverlayView.setVisibility(View.VISIBLE);

                qrCodeReaderView.setQRDecodingEnabled(false);
                String chatId = hUri.getQueryParameter("channelId");
                mOverlayView.setVisibility(View.VISIBLE);
                if (chatId != null) {
                    showAddChannelDialog(chatId);
                } else {
                    if (getView() != null)
                        Snackbar.make(getView(), "Parse Error", Snackbar.LENGTH_SHORT).show();
                }
                return;
            }

            if (hUri != null) {
                if (hUri.getAuthority().contentEquals("linkappmessenger.com")
                        && hUri.getLastPathSegment().contentEquals("channels")
                        && hUri.getQueryParameter("channelId") != null) {
                    mOverlayView.setVisibility(View.VISIBLE);

                    qrCodeReaderView.setQRDecodingEnabled(false);

                    String channelId = hUri.getQueryParameter("channelId");
                    showAddChannelDialog(channelId);
                    System.out.println("Channel: " + channelId);
                }

            }

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }


    public interface OnFragmentInteractionListener {
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(View dialogView, boolean b) {


        ViewGroup.LayoutParams layParamsGet = revealLayout.getLayoutParams();

        int w = getView().getWidth();
        int h = layParamsGet.height;

        int endRadius = (int) Math.hypot(w, h);

        int cx = (int) (fabX + (fabWidth / 2));
        int cy = (int) (fabY + fabHeight + 56);

        //revealLayout.setVisibility(View.INVISIBLE);
        if (b) {
            Animator revealAnimator = ViewAnimationUtils.createCircularReveal(revealLayout, cx, cy, (float) fabHeight / 2, endRadius);

            revealLayout.setVisibility(View.VISIBLE);
            revealAnimator.setDuration(5000);
            revealAnimator.start();

        } else {

            Animator anim =
                    ViewAnimationUtils.createCircularReveal(revealLayout, cx, cy, endRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    revealLayout.setVisibility(View.INVISIBLE);

                }
            });
            anim.setDuration(700);
            anim.start();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddChatDialogClosedEvent(AddChatDialogClosedEvent event) {
        qrCodeReaderView.setQRDecodingEnabled(true);
    }

    private void readQRCodeFromImageLibrary(Uri selectedImage) {

        InputStream imageStream = null;
        try {
            //getting the image
            imageStream = getContext().getContentResolver().openInputStream(selectedImage);
        } catch (FileNotFoundException e) {
            Toast.makeText(getContext(), "File not found", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        //decoding bitmap
        Bitmap bMap = BitmapFactory.decodeStream(imageStream);
        int[] intArray = new int[bMap.getWidth() * bMap.getHeight()];
        // copy pixel data from the Bitmap into the 'intArray' array
        bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(),
                bMap.getHeight());

        LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(),
                bMap.getHeight(), intArray);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Reader reader = new MultiFormatReader();// use this otherwise
        // ChecksumException
        try {
            Hashtable<DecodeHintType, Object> decodeHints = new Hashtable<DecodeHintType, Object>();
            decodeHints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            decodeHints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

            Result result = reader.decode(bitmap);
            //*I have created a global string variable by the name of barcode to easily manipulate data across the application*//
            barcode = result.getText();

            //do something with the results for demo i created a popup dialog
            if (barcode != null) {
                Uri hUri = Uri.parse(barcode);
                if (barcode.startsWith("https://linkappmessenger.com/?channelId=")) {

                    String chatId = hUri.getQueryParameter("channelId");
                    mOverlayView.setVisibility(View.VISIBLE);
                    if (chatId != null) {
                        showAddChannelDialog(chatId);
                    } else {
                        if (getView() != null)
                            Snackbar.make(getView(), "Parse Error", Snackbar.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (hUri != null) {
                    if (hUri.getAuthority().contentEquals("linkappmessenger.com")
                            && hUri.getLastPathSegment().contentEquals("channels")
                            && hUri.getQueryParameter("channelId") != null) {
                        String channelId = hUri.getQueryParameter("channelId");
                        showAddChannelDialog(channelId);
                        System.out.println("Channel: " + channelId);
                    }

                }


            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Scan Result");
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setMessage("Nothing found try a different image or try again");
                AlertDialog alert1 = builder.create();
                alert1.setButton(DialogInterface.BUTTON_POSITIVE, "Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                alert1.setCanceledOnTouchOutside(false);

                alert1.show();

            }
            //the end of do something with the button statement.

        } catch (NotFoundException e) {
            Toast.makeText(getContext(), "Nothing Found", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (ChecksumException e) {
            Toast.makeText(getContext(), "Something weird happen, i was probably tired to solve this issue", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (FormatException e) {
            Toast.makeText(getContext(), "Wrong Barcode/QR format", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (NullPointerException e) {
            Toast.makeText(getContext(), "Something weird happen, i was probably tired to solve this issue", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void showAddChannelDialog(String chatId) {
        mDatabaseReference.child(chatId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mOverlayView.setVisibility(View.INVISIBLE);

                if (dataSnapshot.exists()) {

                    ChatRoom chatRoom = dataSnapshot.getValue(ChatRoom.class);
                    AddChannelFragment.newInstance(chatRoom).show(getChildFragmentManager(), "ADD_CHANNEL");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {

                    final Uri imageUri = imageReturnedIntent.getData();
                    readQRCodeFromImageLibrary(imageUri);

                }
        }
    }

    private void pickImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }
}


