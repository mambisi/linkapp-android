package com.mazeworks.linkapp.ui.home;


import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;

import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.auth.FirebaseAuth;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.util.ApplicationSettingsUtil;
import com.mazeworks.linkapp.util.CapturePhotoUtils;

import net.glxn.qrgen.android.QRCode;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.NOTIFICATION_SERVICE;


public class QRCodeGeneratorFragment extends DialogFragment {
    private static final String TAG = "IMAGE_CREATE";
    private static String QRCODE_TAG = "QR_CODE";
    private static String CHAT_TAG = "CHAT_TITLE";
    private String qrCode = "";
    private String chatTitle = "";
    private ImageButton downloadQrImageButton;
    private AppCompatImageView qrImage;
    private AppCompatTextView mChatCaptionText;
    private CircleImageView mChatImage;
    private CardView mCardView;
    private FloatingActionButton mShareQrCode;
    private Bitmap screenshot;
    private String imageurl;
    private int id = 2002;

    public QRCodeGeneratorFragment() {
        // Required empty public constructor
    }

    public static QRCodeGeneratorFragment newInstance(String qrCode, String chatTitle) {
        QRCodeGeneratorFragment fragment = new QRCodeGeneratorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(QRCODE_TAG, qrCode);
        bundle.putString(CHAT_TAG, chatTitle);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.AppTheme);
        if (getArguments() != null) {
            qrCode = getArguments().getString(QRCODE_TAG);
            chatTitle = getArguments().getString(CHAT_TAG);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        downloadQrImageButton = view.findViewById(R.id.saveQRButton);
        qrImage = view.findViewById(R.id.charQR);
        mChatImage = view.findViewById(R.id.chatImage);
        mCardView = view.findViewById(R.id.cardView);
        mShareQrCode = view.findViewById(R.id.shareQrCode);
        mChatCaptionText = view.findViewById(R.id.chatCaption);

        mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editCaption();
            }
        });

        Uri.Builder qrUri = new Uri.Builder()
                .scheme("https")
                .authority("linkappmessenger.com")
                .appendPath("channels")
                .appendQueryParameter("channelId", qrCode);

        String uriString = qrUri.build().toString();
        Bitmap myBitmap = QRCode.from(uriString).bitmap();
        qrImage.setImageBitmap(myBitmap);

        setupViews();


    }

    private void setupViews() {

        mShareQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (screenshot == null)
                    screenshot = getViewBitmap(mCardView);
                String insertImage = CapturePhotoUtils.insertImage(getContext(), screenshot, chatTitle, new CapturePhotoUtils.Callback() {
                    @Override
                    public void complete() {

                    }
                });
                if (getView() != null)
                    Snackbar.make(getView(), "QR image Saved ", Snackbar.LENGTH_SHORT).show();

                shareImage(Uri.parse(insertImage));
            }
        });


        Glide.with(getActivity()).load(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        TimerTask timerTask = new TimerTask() {
                            @Override
                            public void run() {
                                screenshot = getViewBitmap(mCardView);
                            }
                        };
                        new Timer().schedule(timerTask, 200);
                        return false;
                    }
                }).into(mChatImage);


        downloadQrImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String imageName = "CHAT_ID_" + qrCode;
                PermissionListener permissionlistener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }


                };

                new TedPermission(getContext())
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
                if (screenshot == null)
                    screenshot = getViewBitmap(mCardView);

                imageurl = CapturePhotoUtils.insertImage(getContext(), screenshot, chatTitle, new CapturePhotoUtils.Callback() {
                    @Override
                    public void complete() {

                    }
                });

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    showOnOreoNotification(screenshot);
                } else {
                    showNotification(screenshot);
                }
                if (getView() != null)
                    Snackbar.make(getView(), "QR image Saved ", Snackbar.LENGTH_SHORT).show();


            }
        });
    }

    private void shareImage(Uri bmpUri) {
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            // Launch sharing dialog for image
            startActivity(Intent.createChooser(shareIntent, "Share Image"));
        } else {
            // ...sharing failed, handle error
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qrcode_generator, container, false);
    }

    private void editCaption() {
        new MaterialDialog.Builder(getActivity())
                .title("Edit Caption")
                .inputRangeRes(2, 40, R.color.colorPrimaryDark)
                .input("Caption", mChatCaptionText.getText().toString(), new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        mChatCaptionText.setText(input);
                        screenshot = getViewBitmap(mCardView);
                    }
                }).show();
    }


    private Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            Log.e(TAG, "failed getViewBitmap(" + v + ")", new RuntimeException());
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showOnOreoNotification(Bitmap drawable) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(imageurl), "image/*");
        Notification.Builder mBuilder =
                new Notification.Builder(getActivity())
                        .setSmallIcon(R.drawable.qrcode)
                        .setLargeIcon(drawable)
                        .setAutoCancel(true)
                        .setChannelId(ApplicationSettingsUtil.QR_IMAGE_CREATOR_CHANNEL_ID)
                        .setContentTitle("QR Image Saved");
        int mNotificationId = (int) new Date().getTime();
        PendingIntent contentIntent =
                PendingIntent.getActivity(getContext().getApplicationContext(),
                        mNotificationId,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );
        mBuilder.setContentIntent(contentIntent);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }

    private void showNotification(Bitmap drawable) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(imageurl), "image/*");
        Notification.Builder mBuilder =
                new Notification.Builder(getActivity())
                        .setSmallIcon(R.drawable.qrcode)
                        .setLargeIcon(drawable)
                        .setAutoCancel(true)
                        .setContentTitle("QR Image Saved");
        int mNotificationId = (int) new Date().getTime();
        PendingIntent contentIntent =
                PendingIntent.getActivity(getContext().getApplicationContext(),
                        mNotificationId,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );
        mBuilder.setContentIntent(contentIntent);
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }

}
