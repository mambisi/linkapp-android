package com.mazeworks.linkapp.ui.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.ui.login.EditUserProfileActivity;

import de.hdodenhof.circleimageview.CircleImageView;


public class UserProfileFragment extends Fragment{

    private CircleImageView mProfileImage;
    private AppCompatTextView mUsername;
    private FirebaseUser mFirebaseUser;
    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mProfileImage = view.findViewById(R.id.userProfile);
        mUsername = view.findViewById(R.id.username);

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditUserProfileActivity.class);
                Pair<View, String> p1 = Pair.create((View)mProfileImage, "profile");
                Pair<View, String> p3 = Pair.create((View)mUsername, "displayName");
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), p1, p3);
                startActivity(intent, options.toBundle());
            }
        });

        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (mFirebaseUser != null) {
            RequestOptions requestOptions = RequestOptions.placeholderOf(R.drawable.user_profile_placeholder).fallback(R.drawable.user_profile_placeholder);
            Glide.with(getActivity()).load(mFirebaseUser.getPhotoUrl()).apply(requestOptions).into(mProfileImage);
            mUsername.setText(mFirebaseUser.getDisplayName());
        }
    }

    public String getTitle(){
        return "Profile";
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFirebaseUser != null) {
            Glide.with(getActivity()).load(mFirebaseUser.getPhotoUrl()).into(mProfileImage);
            mUsername.setText(mFirebaseUser.getDisplayName());
        }
    }



}


