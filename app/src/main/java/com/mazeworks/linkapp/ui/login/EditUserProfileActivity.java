package com.mazeworks.linkapp.ui.login;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.ui.MainActivity;

import com.mazeworks.linkapp.ui.settings.SettingsPrefActivity;
import com.mazeworks.linkapp.util.ImageCompression;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditUserProfileActivity extends AppCompatActivity implements ImageCompression.ICompressorListener {
    private CircleImageView mProfileImage;
    private AppCompatEditText mDisplayName;
    private StorageReference storageRef;
    private AppCompatButton mDoneButton;
    private FirebaseUser user;
    private final int SELECT_PHOTO = 10053;
    private boolean mCanPickImage = false;
    private AppCompatImageButton mSettingButton;
    private DatabaseReference mUserDatabaseRef = FirebaseDatabase.getInstance().getReference("users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_user_profile);
        storageRef = FirebaseStorage.getInstance().getReference();


        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                mCanPickImage = true;
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA,Manifest.permission.READ_CONTACTS)
                .check();


        mProfileImage = findViewById(R.id.userProfile);
        mDisplayName = findViewById(R.id.username);
        mDoneButton = findViewById(R.id.doneButton);
        mSettingButton = findViewById(R.id.settingsButton);
        mSettingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditUserProfileActivity.this, SettingsPrefActivity.class));
            }
        });
        user = FirebaseAuth.getInstance().getCurrentUser();

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //changeProfileImage();
                if (mCanPickImage) {
                    pickImage();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission not Granted", Toast.LENGTH_LONG).show();
                }
            }
        });


        if (user != null) {
            mDisplayName.setText(user.getDisplayName());
            RequestOptions requestOptions = RequestOptions.placeholderOf(R.drawable.user_profile_placeholder);
            Glide.with(this).load(user.getPhotoUrl()).apply(requestOptions).into(mProfileImage);
            if (user.getDisplayName() != null) {
                if (!user.getDisplayName().isEmpty()) {
                    mDoneButton.setEnabled(true);
                }
            }
        } else {
            try {
                throw new Exception("USER IS NULL");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        mDisplayName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 2) {
                    if (!mDoneButton.isEnabled())
                        mDoneButton.setEnabled(true);
                } else {
                    if (mDoneButton.isEnabled())
                        mDoneButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserProfileChangeRequest.Builder changeRequest = new UserProfileChangeRequest.Builder();
                changeRequest.setDisplayName(mDisplayName.getText().toString());
                mDoneButton.setEnabled(false);
                mDisplayName.setEnabled(false);
                user.updateProfile(changeRequest.build()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mDoneButton.setEnabled(true);
                        mDoneButton.setEnabled(true);

                        mUserDatabaseRef.child(user.getUid()).child("name").setValue(user.getDisplayName());
                        startActivity(new Intent(EditUserProfileActivity.this, MainActivity.class));
                        finish();
                    }
                });

            }
        });

    }

    private void pickImage() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    private void handleImageSelected(Uri uri) {
        UCrop.Options options = new UCrop.Options();
        options.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        options.setToolbarTitle("Edit Profile");
        options.setToolbarColor(getResources().getColor(R.color.colorPrimaryDark));

        UCrop.of(uri, Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "IMG_CROP" + System.currentTimeMillis() + ".jpg")))
                .withAspectRatio(1, 1)
                .withMaxResultSize(512, 512)
                .withOptions(options)
                .start(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {

                final Uri imageUri = data.getData();
                handleImageSelected(imageUri);

            }

        } else if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            setCroppedImageUri(resultUri);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            cropError.printStackTrace();
        }
    }

    public void setCroppedImageUri(Uri uri) {
        ImageCompression imageCompression = new ImageCompression(this);
        imageCompression.execute(Uri.parse(uri.getSchemeSpecificPart()).toString());
    }

    @Override
    public void onCompressionCompleted(String filePath) {
        uploadToStorage(Uri.fromFile(new File(filePath)));
    }

    private void uploadToStorage(Uri uri) {
        StorageReference riversRef = storageRef.child("images/profiles/" + user.getUid() + "/" + new Date().toString() + ".jpg");

        riversRef.putFile(uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        updateUserProfileUrl(downloadUrl);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                    }
                });
    }

    private void updateUserProfileUrl(final Uri uri) {
        UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder().setPhotoUri(uri).build();
        user.updateProfile(profileChangeRequest).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    try {
                        Glide.with(EditUserProfileActivity.this).load(uri).into(mProfileImage);
                        mUserDatabaseRef.child(user.getUid()).child("profileUrl").setValue(uri.toString());
                    } catch (Exception e) {

                    }

                }
            }
        });
    }

    @Override
    public void compressionStarted() {

    }

}
