package com.mazeworks.linkapp.ui.login;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mazeworks.linkapp.R;

public class EmailLoginFragment extends Fragment {
    private LoginInteractions mLoginInteractions;
    private AppCompatEditText mEmail;
    public EmailLoginFragment() {
        // Required empty public constructor
    }

    public static EmailLoginFragment newInstance() {
        EmailLoginFragment fragment = new EmailLoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        AppCompatButton contBtn = view.findViewById(R.id.continueWithEmailButton);
        mEmail = view.findViewById(R.id.emailEditText);
        contBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continueToPassword();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginInteractions){
            mLoginInteractions = (LoginInteractions) context;
        }
        else {
            try {
                throw new Exception("Context Must Implement LoginInteraction Interface");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void continueToPassword(){
        if (emailIsValid()){
            mLoginInteractions.continueWithEmail(mEmail.getText().toString());
        }
        else {
            if(getView() != null)
            Snackbar.make(getView(), "Invalid Email Address", BaseTransientBottomBar.LENGTH_LONG).show();
            else
                Toast.makeText(getContext(),"Inavlid  Email Address", Toast.LENGTH_LONG).show();
        }

    }

    private boolean emailIsValid(){
        return !TextUtils.isEmpty(mEmail.getText()) && Patterns.EMAIL_ADDRESS.matcher(mEmail.getText()).matches();
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mLoginInteractions = null;
    }


}
