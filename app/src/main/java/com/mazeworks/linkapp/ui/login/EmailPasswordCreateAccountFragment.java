package com.mazeworks.linkapp.ui.login;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mazeworks.linkapp.R;


public class EmailPasswordCreateAccountFragment extends Fragment {

    private LoginInteractions mLoginInteractions;
    private static String EMAIL_TAG = "EMAIL";
    private String mEmail;
    private AppCompatEditText mPassword,mRPassword;
    private TextInputLayout TILPassword;
    private AppCompatButton mCreateAccountButton;
    private String mUserPassword;
    private boolean canCreateAccount = false;
    private ProgressDialog mProgressDialog;

    public EmailPasswordCreateAccountFragment() {
        // Required empty public constructor
    }

    public static EmailPasswordCreateAccountFragment newInstance(String email) {
        EmailPasswordCreateAccountFragment fragment = new EmailPasswordCreateAccountFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EMAIL_TAG, email);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ( getArguments() != null){
            mEmail = getArguments().getString(EMAIL_TAG);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mPassword = view.findViewById(R.id.passwordEditText);
        mRPassword = view.findViewById(R.id.repeatPasswordEditText);
        mCreateAccountButton = view.findViewById(R.id.createAccountButton);
        setUpViews();

    }

    private void setUpViews(){

        mProgressDialog =  new ProgressDialog(getContext());
        mProgressDialog.setMessage("Creating new account");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mCreateAccountButton.setOnClickListener(new ICreateAccount());
        mRPassword.setEnabled(false);
        mCreateAccountButton.setEnabled(false);

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 5 ){
                    mRPassword.setEnabled(true);
                }
                else {
                    if (mRPassword.isEnabled())
                        mRPassword.setEnabled(false);
                }
                if(!charSequence.toString().contentEquals(mRPassword.getText()) ){
                    if (mCreateAccountButton.isEnabled()){
                        mCreateAccountButton.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().contentEquals(mRPassword.getText()) ){
                    mCreateAccountButton.setEnabled(true);
                }
            }
        });

        mRPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.toString().contentEquals(mPassword.getText()) ){
                    mCreateAccountButton.setEnabled(true);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(mPassword.getText().toString().contentEquals(charSequence) ){
                    mCreateAccountButton.setEnabled(true);
                }
                else {
                    if (mCreateAccountButton.isEnabled()){
                        mCreateAccountButton.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mUserPassword = editable.toString();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email_create_account_password, container, false);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginInteractions){
            mLoginInteractions = (LoginInteractions) context;
        }
        else {
            try {
                throw new Exception("Context Must Implement LoginInteraction Interface");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mLoginInteractions = null;
    }

    class ICreateAccount implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            mProgressDialog.show();
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(mEmail, mUserPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {


                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            }).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    mLoginInteractions.userAccountCreated();
                    System.out.println(authResult.getUser().getEmail());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Snackbar.make(getView(), e.getMessage(), BaseTransientBottomBar.LENGTH_LONG).show();
                }
            });
            System.out.println(mUserPassword);
        }
    }

}
