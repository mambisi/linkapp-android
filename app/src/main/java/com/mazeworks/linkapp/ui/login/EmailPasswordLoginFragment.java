package com.mazeworks.linkapp.ui.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.User;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmailPasswordLoginFragment extends Fragment {

    private static String EMAIL_TAG = "EMAIL";
    private String mEmail;
    private CircleImageView mProfileImage;
    private AppCompatTextView mUsername;
    private AppCompatTextView mEmailText;
    private AppCompatButton mLoginButton;
    private AppCompatEditText mPassword;
    private LoginInteractions mLoginInteractions;
    private ProgressDialog mProgressDialog;

    public EmailPasswordLoginFragment() {
        // Required empty public constructor
    }


    public static EmailPasswordLoginFragment newInstance(String email) {
        EmailPasswordLoginFragment fragment = new EmailPasswordLoginFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EMAIL_TAG, email);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mEmail = getArguments().getString(EMAIL_TAG);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mProfileImage = view.findViewById(R.id.userProfile);
        mUsername = view.findViewById(R.id.username);
        mEmailText = view.findViewById(R.id.emailText);
        mEmailText.setText(mEmail);
        mLoginButton = view.findViewById(R.id.createAccountButton);
        mPassword = view.findViewById(R.id.passwordEditText);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Checking Credentials");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getActivity());
                mProgressDialog.show();
                FirebaseAuth.getInstance().signInWithEmailAndPassword(mEmail, mPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                    }
                }).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        mLoginInteractions.userLoggedInSuccessful();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(getView(), e.getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        });
        if (User.UserContext.USER_IN_CONTEXT != null) {
            RequestOptions requestOptions = RequestOptions.placeholderOf(R.drawable.user_profile_placeholder);
            Glide.with(getActivity()).load(User.UserContext.USER_IN_CONTEXT.getProfileUrl()).apply(requestOptions).into(mProfileImage);
            mUsername.setText(User.UserContext.USER_IN_CONTEXT.getName());
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginInteractions) {
            mLoginInteractions = (LoginInteractions) context;
        } else {
            try {
                throw new Exception("Context Must Implement LoginInteraction Interface");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mLoginInteractions = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email_password_login, container, false);
    }

    public void hideKeyboard(Activity activity) {
        View v = activity.getWindow().getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

}
