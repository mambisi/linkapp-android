package com.mazeworks.linkapp.ui.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mazeworks.linkapp.R;
import com.mazeworks.linkapp.model.User;
import com.mazeworks.linkapp.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements LoginInteractions , PhoneLoginFragment.OnFragmentInteractionListener,VerifyCodeFragment.OnFragmentInteractionListener{

    private FrameLayout contentContainerLayout;
    private final EmailLoginFragment mEmailLoginFragment = EmailLoginFragment.newInstance();
    private final PhoneLoginWelcomeFragment mPhoneLoginWelcomeFragment = PhoneLoginWelcomeFragment.newInstance();
    private DatabaseReference mUserDatabaseRef = FirebaseDatabase.getInstance().getReference("users");
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        contentContainerLayout = findViewById(R.id.contentContainer);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(contentContainerLayout.getId(), mPhoneLoginWelcomeFragment).commit();
        mProgressDialog = new ProgressDialog(this);

    }
    private void replaceFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(contentContainerLayout.getId(), fragment).commit();
    }

    @Override
    public void continueWithEmail(final String email) {

        Query findUserByEmailQuery = mUserDatabaseRef.orderByChild("email").equalTo(email).limitToFirst(1);

        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage("Checking Email");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        findUserByEmailQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressDialog.dismiss();
                if (dataSnapshot.getChildrenCount() > 0) {

                    List<User> userList = new ArrayList<>();
                    for (DataSnapshot d :
                            dataSnapshot.getChildren()) {
                        userList.add(d.getValue(User.class));
                    }

                    User.UserContext.USER_IN_CONTEXT = userList.get(0);

                    replaceFragment(EmailPasswordLoginFragment.newInstance(email));
                }

                else {
                    replaceFragment(EmailPasswordCreateAccountFragment.newInstance(email));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressDialog.dismiss();
                Snackbar.make(contentContainerLayout, "INTERNAL ERROR", Snackbar.LENGTH_SHORT).show();
            }
        });




    }

    @Override
    public void userAccountCreated() {
        startActivity(new Intent(this, EditUserProfileActivity.class));
        finish();

    }

    @Override
    public void continueToPhoneVerification() {
        replaceFragment(PhoneLoginFragment.newInstance());
    }

    @Override
    public void userLoggedInSuccessful() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }



    @Override
    public void onVerificationComplete() {

    }

    @Override
    public void onPhoneNumberEntered(String phoneNumber) {
        replaceFragment(VerifyCodeFragment.newInstance(phoneNumber));
    }
}
