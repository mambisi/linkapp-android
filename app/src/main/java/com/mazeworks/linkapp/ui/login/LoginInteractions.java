package com.mazeworks.linkapp.ui.login;

/**
 * Created by mambisiz on 9/2/17.
 */


public interface LoginInteractions{
    void continueWithEmail(String email);
    void userAccountCreated();
    void continueToPhoneVerification();
    void userLoggedInSuccessful();
}