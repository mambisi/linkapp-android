package com.mazeworks.linkapp.ui.login;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mazeworks.linkapp.R;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import java.util.concurrent.TimeUnit;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PhoneLoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PhoneLoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhoneLoginFragment extends Fragment {

    private View mCountryPicker;
    private CircleImageView mCountryIcon;
    private AppCompatTextView mCountryName;
    private AppCompatTextView mCountryCode;
    private CircularProgressButton mVerifyButton;
    private AppCompatEditText mPhoneNumber;
    private CountryPicker countryPicker = new CountryPicker();
    private Country country;
    private OnFragmentInteractionListener mListener;
    private String formattedPhone = "";
    private PhoneNumberFormattingTextWatcher phoneFormatWatch;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    public PhoneLoginFragment() {
        // Required empty public constructor
    }


    public static PhoneLoginFragment newInstance() {
        PhoneLoginFragment fragment = new PhoneLoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_phone_login, container, false);
        mCountryPicker = view.findViewById(R.id.countryPicker);
        mCountryIcon = view.findViewById(R.id.countFlag);
        mCountryCode = view.findViewById(R.id.countryPhoneCode);
        mVerifyButton = view.findViewById(R.id.phoneLoginButton);
        mCountryName = view.findViewById(R.id.countryName);
        mPhoneNumber = view.findViewById(R.id.userPhone);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mPhoneNumber.setFocusableInTouchMode(true);
        mPhoneNumber.requestFocus();

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mPhoneNumber, InputMethodManager.SHOW_IMPLICIT);


        country = Country.getCountryFromSIM(getContext());
        updateCountryFields(country);
        countryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                country = Country.getCountryByISO(code);
                updateCountryFields(country);
                countryPicker.dismiss();
            }
        });

        mCountryPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryPicker.show(getChildFragmentManager(), "COUNTRY_PICKER");
            }
        });

        mVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (phoneNumberVerified()) {
                    String stringHtml = "Please, are sure you want to send the verification code to <b>" + formattedPhone + "</b> ?";


                    new MaterialDialog.Builder(getContext())
                            .title("Phone Confirmation")
                            .content(Html.fromHtml(stringHtml))
                            .positiveText("Yes")
                            .negativeText("No")
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mCountryPicker.setClickable(false);
                                    mPhoneNumber.setEnabled(false);
                                    mVerifyButton.startAnimation();
                                    mListener.onPhoneNumberEntered(formattedPhone);
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mPhoneNumber.getText().clear();
                                }
                            })
                            .show();
                } else {
                    Snackbar.make(getView(), "Check phone number", Snackbar.LENGTH_SHORT).show();
                }
            }
        });


    }

    private boolean phoneNumberVerified() {
        String swissNumberStr = mPhoneNumber.getText().toString();
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phone = phoneUtil.parse(swissNumberStr, country.getCode());
            if (phoneUtil.isValidNumber(phone)) {
                formattedPhone = phoneUtil.format(phone, PhoneNumberUtil.PhoneNumberFormat.E164);
            }
            return phoneUtil.isValidNumber(phone);

        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            return false;
        }

    }

    private void updateCountryFields(Country country) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            phoneFormatWatch = new PhoneNumberFormattingTextWatcher(country.getCode());
            mPhoneNumber.addTextChangedListener(phoneFormatWatch);
        }
        mCountryName.setText(country.getName().toUpperCase());
        mCountryCode.setText(country.getDialCode());
        String cnt = country.getName().toLowerCase().replace(" ", "_").replace("-", "_");
        String uri = "drawable/" + cnt;

        int imageResource = getResources().getIdentifier(uri, null, getContext().getPackageName());
        if (imageResource > 0) {
            Drawable image = getResources().getDrawable(imageResource);
            mCountryIcon.setImageDrawable(image);
        } else if (imageResource == 0) {
            Drawable drawable = getContext().getResources().getDrawable(country.getFlag());
            mCountryIcon.setImageDrawable(drawable);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onPhoneNumberEntered(String phoneNumber);
    }
}
