package com.mazeworks.linkapp.ui.login;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mazeworks.linkapp.R;

public class PhoneLoginWelcomeFragment extends Fragment {
    private LoginInteractions mLoginInteractions;

    public PhoneLoginWelcomeFragment() {
        // Required empty public constructor
    }

    public static PhoneLoginWelcomeFragment newInstance() {
        PhoneLoginWelcomeFragment fragment = new PhoneLoginWelcomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_phone_welcome, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        AppCompatButton contBtn = view.findViewById(R.id.phoneLoginGetStartedButton);
        contBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continueToPassword();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginInteractions) {
            mLoginInteractions = (LoginInteractions) context;
        } else {
            try {
                throw new Exception("Context Must Implement LoginInteraction Interface");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void continueToPassword() {
        mLoginInteractions.continueToPhoneVerification();


    }


    @Override
    public void onDetach() {
        super.onDetach();
        mLoginInteractions = null;
    }


}
