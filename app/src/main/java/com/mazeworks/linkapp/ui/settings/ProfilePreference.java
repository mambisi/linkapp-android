package com.mazeworks.linkapp.ui.settings;

import android.content.Context;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.preference.Preference;

import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.mazeworks.linkapp.R;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by mambisiz on 9/7/17.
 */

public class ProfilePreference extends Preference {
    private CircleImageView mProfileImage;
    private AppCompatTextView mSummaryText;
    private Drawable mImageRes;
    private String mCaption;

    public ProfilePreference(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ProfilePreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs,defStyle);

        setWidgetLayoutResource(R.layout.profile_pref_layout);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProfilePreference, defStyle, 0);

        mImageRes = a.getDrawable(R.styleable.ProfilePreference_image);
        mCaption = a.getString(R.styleable.ProfilePreference_caption);

        a.recycle();
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        mProfileImage = view.findViewById(R.id.profile_image);
        mSummaryText = view.findViewById(R.id.profile_pref_summary);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser != null){
            RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.user_profile_placeholder);
            Glide.with(getContext()).load(firebaseUser.getPhotoUrl()).apply(requestOptions).into(mProfileImage);
        }
        else {
            mProfileImage.setImageDrawable(mImageRes);
        }

        mSummaryText.setText(mCaption);
    }
}
