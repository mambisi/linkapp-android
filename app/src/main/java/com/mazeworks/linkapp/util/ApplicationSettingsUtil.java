package com.mazeworks.linkapp.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by mambisiz on 9/12/17.
 */

public class ApplicationSettingsUtil {

    public static String QR_IMAGE_CREATOR_CHANNEL_ID = "qr_image_notify";
    private static String QR_IMAGE_CREATOR_CHANNEL_NAME = "QR Image Creator";
    public static String IMAGE_UPLOADER_CHANNEL_ID = "image_uploader_notify";
    private static String IMAGE_UPLOADER_CHANNEL_NAME = "Upload Image";


    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void createNotificationChannels(Context context){
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = QR_IMAGE_CREATOR_CHANNEL_ID;
        CharSequence channelName = QR_IMAGE_CREATOR_CHANNEL_NAME;
        int importance = NotificationManager.IMPORTANCE_MAX;
        NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        notificationManager.createNotificationChannel(notificationChannel);


        NotificationChannel uploadImageNotificationChannel = new NotificationChannel(IMAGE_UPLOADER_CHANNEL_ID, IMAGE_UPLOADER_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
        uploadImageNotificationChannel.setLightColor(Color.RED);
        notificationManager.createNotificationChannel(uploadImageNotificationChannel);
    }

    public static void sendRegistrationToServer(String refreshedToken) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {

            FirebaseDatabase.getInstance().getReference("users").child(firebaseUser.getUid()).child("messengerToken").setValue(refreshedToken);
        }
    }

}
