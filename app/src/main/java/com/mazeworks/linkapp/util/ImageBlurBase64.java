package com.mazeworks.linkapp.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;


import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by mambisiz on 9/21/17.
 */

public class ImageBlurBase64 extends AsyncTask<Void, Void, String> {


    private Context mContext;
    private Bitmap mOriginal;
    private ImageBlurCallback mCallback;


    @Override
    protected String doInBackground(Void... voids) {
        String encodedImage = convertToBase64(mContext,mOriginal);
        return encodedImage;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mCallback.onDoneWithBlur(s);
    }

    public ImageBlurBase64(Context context, Bitmap original, ImageBlurCallback callback) {
        mContext = context;
        mOriginal = original;
        mCallback = callback;
    }

    private String convertToBase64(Context context, Bitmap original) {
        Bitmap bluredImage = blur(context, original);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bluredImage.compress(Bitmap.CompressFormat.WEBP, 2, baos);
        byte[] b = baos.toByteArray();
        try {
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    private static final float BITMAP_SCALE = 0.05f;
    private static final float BLUR_RADIUS = 23.5f;

    private static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }




    public interface ImageBlurCallback {
        void onDoneWithBlur(String string);
    }

}
